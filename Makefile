SRCFILES=$(wildcard ./src/*.ts)
TSC=tsc
BROW=broserify
TEST_RUNNER=mocha
TEST_RUNNER_ARGS=

build:
	$(TSC) -p src

test:
	$(TEST_RUNNER) $(TEST_RUNNER_ARGS)

release: build
	npm pack

$(PROTOCOL_DEFINITION): $(SCHEMAS)
	node bin/genProtocol

all: build test release

init:
	npm install
	typings install

publish:
	npm version prerelease -m "PUBLISH %%s to npm."
	npm publish .

.PHONY: build test release
