module.exports = function(grunt) {

    grunt.initConfig({
		execute: {
			gents: {
				src: ['build/gents.js']
			},
			genprot: {
				options: {
					args: ['./etp/build/genProtocol', '-p', '--outputProtocolFile=./etp.avpr', '--schemas=./etp/src/Schemas']
				},
				src: ['./etp/build/genProtocol.js']
			},
			genschema: {
				options: {
					args: ['./etp/build/genProtocol', '-s', '--outputJavaScriptFile=./lib/EtpSchemas.js', '--schemas=./etp/src/Schemas']
				},
				src: ['./etp/build/genProtocol.js']
			},
		},
		mochaTest: {
			default: {
				options: {
					
				},
				src: ['test/**/*.js']
			},
			xunit: {
				options: {
				  reporter: 'xunit',
				  captureFile: 'temp/xunit.xml'
				},
				src: ['test/**/*.js']
			  }
		},
        ts: {
            default : {
                tsconfig: "./src"
            }
        },
        typings: {
            install: {}
        },
        watch: {
        }

    });


    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-execute');
	grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-typings');

    grunt.registerTask('init', ['typings']);
    grunt.registerTask('build', ['ts']);
	grunt.registerTask('test', ['mochaTest:default']);
	// xunit output for CI
	grunt.registerTask('xunit', ['mochaTest:xunit']);
	grunt.registerTask('all', ['init', 'build', 'test']);
	
    grunt.registerTask('default', ['build']);
};