/*
 *License notice
 *
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement.
 *
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License.
 *
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the
 * License.
 *
 * All rights reserved.
 *
 */

var assert = require("assert"),
    should = require("should"),
    path = require("path"),
    fs = require("fs"),
    testModule = require('../../dist/lib/common/DataObjectFactory');

var appRoot = path.dirname(path.dirname(path.dirname(module.filename)));

describe('DataObjectFactory', function(){
    it("Should recognize 'application/x-witsml+xml;version=2.0;type=obj_well' as witsml20", function(){
        var x = new testModule.DataObjectFactory("application/x-witsml+xml;version=2.0;type=obj_well");
        x.packageName.should.equal("witsml20");
    });
    it("Should recognize 'application/x-witsml+xml;version=1.4.1.1;type=obj_well' as witsml141", function(){
        var x = new testModule.DataObjectFactory("application/x-witsml+xml;version=1.4.1.1;type=obj_well");
        x.packageName.should.equal("witsml1411");
    });
    it("Should support witsml1411", function(){
        var x = new testModule.DataObjectFactory("application/x-witsml+xml;version=1.4.1.1;type=obj_well");
        x.packageName.should.equal("witsml1411");
        should.exist(x.getPackage());
    });
    it("Should support witsml20", function(){
        var x = new testModule.DataObjectFactory("application/x-witsml+xml;version=2.0;type=obj_well");
        x.packageName.should.equal("witsml20");
        should.exist(x.getPackage());
    });
    it("Should support prodml122", function(){
        var x = new testModule.DataObjectFactory("application/x-prodml+xml;version=1.2.2;type=obj_wellTest");
        x.packageName.should.equal("prodml122");
        should.exist(x.getPackage());
    });
    it("Should support resqml20", function(){
        var x = new testModule.DataObjectFactory("application/x-resqml+xml;version=2.0;type=obj_TectonicFeature");
        x.packageName.should.equal("resqml20");
        should.exist(x.getPackage());
    });
});

function describeSampleXml(result) {
    describe("The returned value", function(){
        it("Should be exactly one well", function(){
            result.value.well.length.should.equal(1);
        });
        var theWell = result.value.well[0];
        it("Should have the right name", function(){
            theWell.name.should.equal("DemoWell01");
        });
        it("Should have the right uid", function(){
            theWell.uid.should.equal("1d8af245-8da9-4ece-86cd-e115e9c88a3e");
        });
        it("Should map to Xml package witsml1411", function(){
            theWell.TYPE_NAME.should.equal("witsml1411.ObjWell");
        });
    });

}

describe('DataObjectFactory', function(){
    var ct = "application/x-witsml+xml;version=1.4.1.1;type=obj_well";
    var x = new testModule.DataObjectFactory(ct);
    describe('#deserializeString', function(){
        var str = fs.readFileSync(appRoot + "/test/data/well1411.xml", "utf-8");
        describeSampleXml(x.deserializeString(str));
    });
    describe('#deserializeFile', function(){
        var result;

        // asych test
        beforeEach(function(done){
            x.deserializeFile(appRoot + "/test/data/well1411.xml", function(data){
                result=data;
                done();
            });
        });

        it("Should match the sample data", function(){
            describeSampleXml(result);

            describe("#contentTypeFromNative", function(){
                it("Should produce the same content type", function(){
                    testModule.DataObjectFactory.contentTypeFromNative(result.value.well[0]).should.equal(ct);
                });
            })
        });


    });
});



describe('DataObjectFactory', function(){
    var x = new testModule.DataObjectFactory("application/x-witsml+xml;version=1.4.1.1;type=obj_well");
    describe('#serializeString', function(){
        var well = {
            TYPE_NAME: "witsml1411.ObjWell",
            name: "DemoWell01",
            uid: "1d8af245-8da9-4ece-86cd-e115e9c88a3e",
            wellLocation: [{
                latitude: { uom:"dega", value: -28.33823 },
                longitude: { uom:"dega", value: 137.070923 }
            }]
        };

        it("Should serialize javascript hashes", function(){
            var str = x.serializeString(well);
        });

    });
});

