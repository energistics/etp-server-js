/*
 *License notice
 *
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement.
 *
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License.
 *
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the
 * License.
 *
 * All rights reserved.
 *
 */

/*global it, describe */
var assert = require("assert"),
    should = require("should"),
    ETPCore = require('../../dist/lib/common/ETPCore');

var core = new ETPCore.ETPCore();

describe('ETPCore', function () {
    describe('#createHeader()', function () {
        it("Create a message header from arguments", function () {
            var header = core.createHeader(0, 1, 2, 3);
            header.protocol.should.equal(0);
            header.messageType.should.equal(1);
            header.correlationId.should.equal(2);
            header.messageFlags.should.equal(3);
        }.bind(this));
    });
});

