/*
 *License notice
 *
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement.
 *
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License.
 *
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the
 * License.
 *
 * All rights reserved.
 *
 */

/*global it, describe */
var assert = require("assert"),
    should = require("should"),
    path = require("path"),
    fs = require("fs"),
    testModule = require('../../dist/lib/common/EtpUri');

var appRoot = path.dirname(path.dirname(module.filename));

describe('EtpUri', function(){
    it("Should parse a root", function(){
        var uri = new testModule.EtpUri("/");
        uri.isRoot.should.equal(true);
        uri.isUuid.should.equal(false);
        uri.isUrn.should.equal(false);
    });
    it("Should parse eml:///witsml141", function(){
        var uri = new testModule.EtpUri("eml:///witsml141");
        uri.isWitsml.should.equal(true);
        uri.hasDataspace.should.equal(false);
        //should.equal(uri.hasPlural, true);
    });
    it("Should recognize dataspaces", function(){
        new testModule.EtpUri("eml:///witsml141").hasDataspace.should.equal(false);
        new testModule.EtpUri("eml://abc/witsml141").hasDataspace.should.equal(true);
        new testModule.EtpUri("eml://abc/witsml141").dataSpace.should.equal("abc");
        new testModule.EtpUri("eml://a/b/c/witsml141").dataSpace.should.equal("a/b/c");
        new testModule.EtpUri("eml://abc.abc.abc/witsml141").dataSpace.should.equal("abc.abc.abc");
    });
    it("Should recognize schema families", function(){
        new testModule.EtpUri("eml:///witsml141").schemaFamily.should.equal("witsml");
        new testModule.EtpUri("eml:///resqml20").schemaFamily.should.equal("resqml");
    });
    it("Should recognize collections in a URI namespace protocol", function(){
        var uri = new testModule.EtpUri("eml:///witsml141/obj_well");
        uri.schemaFamily.should.equal("witsml");
        uri.objectType.should.equal("obj_well");
        uri.collection.should.equal("well");
        should.equal(uri.identifier, null);
    });
    it("Should capture the id of an object in a collection", function(){
        var uri = new testModule.EtpUri("eml:///witsml141/obj_well(1d8af245-8da9-4ece-86cd-e115e9c88a3e)");
        uri.schemaFamily.should.equal("witsml");
        uri.objectType.should.equal("obj_well");
        uri.collection.should.equal("well");
        should.equal(uri.identifier, "1d8af245-8da9-4ece-86cd-e115e9c88a3e");
    });
});

