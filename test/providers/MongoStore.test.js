var assert = require("assert"),
    fs = require('fs'),
    path = require('path'),
    should = require("should"),
    mongo = require('../../dist/lib/providers/MongoStore'),
    etp =  require('../../dist/lib/Energistics');

var appRoot = path.dirname(path.dirname(path.dirname(module.filename)));

var store;

describe('MongoStore', function () {

	describe("#constructor", function() {
		store = new mongo({databaseConnectString: 'mongodb://localhost:27017/testModule-sample-test'});
		it("Should connect to the local store.", function(){
			should.exist(store);
		});
	});

    describe("#drop", function(){
        this.timeout(5000);
        var result = false;
        beforeEach(function(done){
            store.db.dropDatabase(done);
        });
        it("Should drop the test database before continuing", function(){
            this.timeout(5000);
        });
    });

	describe('#enum()', function () {
		var result = false;

		// asych test
		beforeEach(function(done){
			store.enum("/", function(err, data){
				result = data;
				// complete the async beforeEach
				done();
			});

		});

		it("Should handle the root URI", function(){
			result.length.should.equal(1);
			result[0].uri.should.equal("eml:///witsml1411");
		});
	});
});


describe('MongoStore', function () {

	describe("#put()", function(){
        var str = fs.readFileSync(appRoot + "/test/data/well1411.xml", "utf-8");
        var resource = {
            contentType: "application/x-witsml+xml;version=1.4.1.1;type=obj_well"
        };
        var messageData = {
			header : {},
			body: {
                data: {
                    resource: resource,
                    contentEncoding: "text/xml",
                    data: str
                }
			}
        };
        var errResult;
        var dataResult;

        beforeEach(function(done){
            store.put(messageData, function(err, data){
                errResult = err;
                dataResult = data;
                done();
            });

        });

        describe("The result", function(){
            it("Should not produce an error", function(){
                should.not.exist(errResult);
            });
            it("Should insert one well", function(){

            });
        });
    });

	describe("#get('eml:///witsml1411/obj_well(1d8af245-8da9-4ece-86cd-e115e9c88a3e)')", function () {
		var result = false;
		var theWell = null;

		// async test
		beforeEach(function(done){
			store.get('eml:///witsml1411/obj_well(1d8af245-8da9-4ece-86cd-e115e9c88a3e)', function(err, data){
				result = err?err:data;
				theWell = err?err:data.native;
				done();
			});
		});

		describe("The returned value", function(){
			it("Should have the right name", function(){
				theWell.name.should.equal("DemoWell01");
			});
			it("Should have the right _id", function(){
				theWell._id.should.equal("1d8af245-8da9-4ece-86cd-e115e9c88a3e");
			});
			it("Should map to Xml package witsml1411", function(){

				theWell.TYPE_NAME.should.equal("witsml1411.ObjWell");
			});
		});
	});

});
