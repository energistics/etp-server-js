/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */


/**
 * loadAll.js
 * Loads the energistics sample witsml data set into a mongo back end.
 *
 * This script is fairly dedicate to the exact directory layout and file naming
 * scheme used in the sample data set, though it could be easily tweaked to be
 * more generic.
 *
 * usage: node bin/loader --help
 */

var startTime: number = new Date().valueOf();

import events = require("events");

var fork = require('child_process').fork,
    argv = require('optimist')
        .usage("\nETP Mongo Data Loader for Node.js")
        .describe("workers", "Number of worker processes to fork at once.")
        .describe("files", "Path to the root of the demo data, or any one well in the data set.")
        .describe("schemaModule", "Name of the Jsonix-compiled module of schema bindings.")
        .describe("storeModule", "Name of store implementation file, don't change this.")
        .describe("loaderModule", "Name of the module to call for each file.")
        .describe("databaseConnectString", "Mongo connection string, including database.")
        .default(
        {
            databaseConnectString: 'mongodb://localhost:27017/test',
            schemaModule: 'witsml1411',
            storeModule: '../lib/providers/MongoStore',
            loaderModule: "./bin/loadOne.js",
            files: 'c:/WITSMLSIG_SampleDataSet_1411',
            filePattern: "\\.xml$",
            headerOnly: false,
            dryRun: false,
            workers: 10
        }
    ).argv;

if (argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}

var processes=[];

/// Array of xml file names found in the folder
var all_files = [];
var root = argv.files;
var loader = null;

var stats = {
    dataObjects: 0,
    channels: 0,
    stations: 0,
    rows: 0,
    points: 0,
    nullValues: 0
};

function start_one(filename, slot)
{
    var f = fork(argv.loaderModule, [
        "--fileName", filename,
        "--databaseConnectString", argv.databaseConnectString,
        "--headerOnly", argv.headerOnly,
        "--schemaModule", argv.schemaModule,
        "--dryRun", argv.dryRun,
    ]);
    f.on('message', function(m){
        stats.dataObjects += m.dataObjects;
        stats.rows += m.rows;
        stats.points += m.points;
        stats.stations += m.stations;
        stats.channels += m.channels;
        stats.nullValues += m.nullValues;
    });
    f.on('close', function (code) {
        processes[this.slot]=null;
    }.bind({ slot: slot, filename: filename, start: new Date() }));
    return f;
}

function manage_queue()
{
    var done = true;
    for(var i=0; i<argv.workers; i++){
        if(processes[i]==null){
            if(all_files.length>0) {
                done = false;
                var filename = all_files.shift();
                processes[i]=start_one(filename, i);
            }
        }
        else {
            done = false;
        }
    }
    if(done){
        var endTime : number = new Date().valueOf();
        console.log(new Date() + " Done!");
        console.log(JSON.stringify(stats));
        console.log("Total time: " + ((endTime-startTime) / 60000).toFixed(2) + " Minutes." );
        process.exit(0);
    }
}

var walk = require('walk'),
	fs = require('fs'),
	options,
	walker;

options = {
	followLinks: false
};

function walk_files(argv) {
	walker = walk.walk(root, options);
	walker.on("names", function (root, nodeNamesArray, next) {
		for (var i = 0; i < nodeNamesArray.length; i++) {
			var file = nodeNamesArray[i];
			if (file.match(argv.filePattern))
                all_files.push(root + "/" + file);
			next();
		}
	});

    walker.on("end", function (root, dirStatsArray, next) {
        all_files.sort();
        if (all_files.length>0) {
            console.log(new Date() + " Loading " + all_files.length + " xml files.");
            setInterval(manage_queue, 30);
        }
        else {
            console.log("No files found in [" + argv.files + "]");
            process.exit(0);
        }
    });
}

walk_files(argv);
