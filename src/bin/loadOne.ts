/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

/**
 * Loads a single WITSML file into the mongo store.
 * Normally would be forked from loadAll.ts
 * 
 */

import witsml=require("../lib/common/WitsmlHelper");

var argv = require('optimist')
    .usage("\nWITSML Data Loader for ETP and Node.js")
    .default(
    {
        databaseConnectString: 'mongodb://localhost:27017/test',
        schemaModule: "witsml1411",
        storeModule: '../lib/providers/MongoStore',
        pivot: false,
        headerOnly: false,
        dryRun: false,
        fileName: "H:\\data\\energyml\\WITSMLSIG_SampleDataSet_1411\\DemoWell01\\Wellbore A\\log_L-499007-Time_segment001.xml",
    }
).argv;

if(argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}

function do_work(store)
{
    console.log(argv.fileName);
    var loader = new witsml.FileLoader(store, argv);
    loader.headerOnly = argv.headerOnly;
    loader.on('ready', function () {
        loader.load_doc(argv.fileName);
    });
    loader.on('done', function () {
        if(process.send){
            process.send({dataObjects: 1, rows: loader.rows, points: loader.points, channels: loader.channels, stations: loader.stations, nullValues: loader.nullValues})
        }
        else {
            console.dir({dataObjects: 1, rows: loader.rows, points: loader.points, channels: loader.channels, stations: loader.stations, nullValues: loader.nullValues});
        }
        process.exit(0);
    });
    loader.start();
}

if(argv.dryRun!='true'){
    var storeClass = require(argv.storeModule);
    var store = new storeClass(argv);
    store.on("connect", function(){
        do_work(store);
    });
}
else {
    do_work(null);
}

