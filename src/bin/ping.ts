/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import * as etp from "../lib/Energistics"

/// Use the w3c-style so we can use the web-style API that ETPClient uses.
var socket = require('websocket').w3cwebsocket;
var stats = {
    discoveryUris: 0
};

var
    path=require('path'),
    config = require('optimist')
        .usage("\nETP Server Ping Utility")
        .default(
            {
                traceMessages: false,
                traceDirectory: 'trace',
                configDirectory: '../config',
                recorderFile: 'recorders.json',
                walkDiscoveryTree: true,
                getDataObjects: false,
                rootUri: "/",
                name: '',
				consumer: true

            }
        ).argv;

if(config.help) {
    require('optimist').showHelp();
    process.exit(0);
}

let _connected: boolean = false;
let _resources : any= {};
let _etp : etp.ETPClient=new etp.ETPClient(config);       // Our client session
let _streaming: etp.ChannelStreamingConsumer = new etp.ChannelStreamingConsumer(_etp);
let _discovery : etp.DiscoveryCustomer = new etp.DiscoveryCustomer(_etp);
let _store : etp.StoreCustomer = new etp.StoreCustomer(_etp);

const servers = require(config.configDirectory + "/" + config.recorderFile);
let server = servers[0];
if (config.name!='') {
    for(let i=0; i<servers.length; i++) {
        if(config.name==servers[i].name)
            server=servers[i];
    }
}

function bin2string (array) {
    var result = "";
    for (var i = 0; i < array.length; i++) {
        result += String.fromCharCode(array[i]);
    }
    return result;
}

function onSocketConnect(socket) {
    _connected = true;
    console.log('Socket Status: ' + socket.readyState);
    console.log("Requesting session with {" + server.url + "}");
    var thisVersion = {major: 1, minor: 0, revision: 0, patch: 0};
    _etp.requestSession([
        { protocol: 1, protocolVersion: thisVersion, role: "producer", protocolCapabilities: {}  },
        { protocol: 3, protocolVersion: thisVersion, role: "store", protocolCapabilities: {}  },
        { protocol: 4, protocolVersion: thisVersion, role: "store", protocolCapabilities: {}  }
    ], "etp-ping");
}

/// Socket was disconnected.
function onSocketDisconnect(socket) {
    console.log("Disconnected");
    _connected = false;
}

function onOpenSession(header, message) {
    if (config.walkDiscoveryTree) {
        _discovery.get(config.rootUri);
    }
	if(config.consumer) {
		_streaming.start();
	}
}

function onDiscoverResource(header, message) {
    if(!_resources[message.resource.uri]) {
        console.log(message.resource.uri);
        _resources[message.resource.uri] = message;
        if(message.resource.hasChildren != 0)
            _discovery.get(message.resource.uri);
        if(config.getDataObjects) {
            _store.get(message.resource.uri);
        }
    }
}

function onDataObject(header, message) {
    var uri = new etp.EtpUri(message.dataObject.resource.uri);
    var xml = bin2string(message.dataObject.data);
    console.log(xml);
    //var oParser = new DOMParser();
    //var oDOM = oParser.parseFromString(xml, "text/xml");
}

console.log(config);

console.log(server);                                                                                                               

_etp.on('log', console.log);
_etp.on('connect', onSocketConnect);
_etp.on('disconnect', onSocketDisconnect);
_etp.on('open', onOpenSession);

if (config.consumer) {
	_etp.registerHandler(1, _streaming);
	_streaming.on('metadata', function(message){
		console.dir(message.body);
	});
	_streaming.on('data', function(message){
		console.dir("Received " + message.body.data.length + " points at " + new Date());
	});
}

_etp.registerHandler(3, _discovery);
_discovery.on('resource', onDiscoverResource);

_etp.registerHandler(4, _store);
_store.on('object', onDataObject);

_etp.connect(server, socket);
//_etp.closeSession();