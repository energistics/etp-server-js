/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import * as etp from '../lib/Energistics';
import * as perfmon from '../lib/providers/PerfMonProvider';

var path=require('path'),
    http=require('http'),
    https=require('https'),
    fs=require('fs'),
	WebSocketServer = require('websocket').server,
    // For demo, this is a simple way to get configuration for CCore
    argv = require('optimist')
        .usage("\nPerfMon ChannelStreamingProducer for Node.js")
        .default(
            {
			    wsPort: 8082,
			    simpleStreamer: false,
                skipDuplicates: false,
                traceMessages: false,
                traceDirectory: 'trace',
			    defaultUri: "\\processor(_total)\\% processor time",
                configDirectory: '../config',
                pfxFile: '',
                passphrase: '',
                useTls: false,
				tlsKey: '',
				tlsCert: '',
				tlsCa: '',
				tlsRequestCert: false,
				tlsRejectUnauthorized: true
            }
        )
		.argv;


if(argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}


var server;
if (argv.useTls) {
    var sslConfig = {
        key: fs.readFileSync(argv.tlsKey),
		cert: fs.readFileSync(argv.tlsCert),
		ca: fs.readFileSync(argv.tlsCa),
		requestCert: argv.tlsRequestCert,
		rejectUnauthorized: argv.tlsRejectUnauthorized
    };

    server = https.createServer(sslConfig, function(request, response) {
        console.log((new Date()) + ' Received request for ' + request.url);
        response.writeHead(404);
        response.end();
    });
}
else{
    // Create another server for the websocket traffic.
    server = http.createServer(function(request, response) {
        console.log((new Date()) + ' Received request for ' + request.url);
        response.writeHead(404);
        response.end();
    });

}
server.listen(argv.wsPort, function() {
    console.log((new Date()) + ' Server is listening on port '+argv.wsPort);
});

var wsServer = new WebSocketServer({httpServer: server});

var g_Connections=[];

wsServer.on('request', function (request: any) {

    // For web clients, copy query string parameters to headers.
    for(var queryParameter in request.resourceURL.query)  {
        request.httpRequest.headers[queryParameter.toLowerCase()] = request.resourceURL.query[queryParameter];
    }

    if(request.httpRequest.headers['etp-sessionid'] != null) {
        request.reject(410);
    } else {
        var ralf = new etp.ServerSession(argv, null);
        var producer =  new PerfMonProducer(ralf);
        var store = new perfmon.PerfStore(".");
        ralf.registerHandler(1, producer);
        if (!argv.simpleStreamer) {
            ralf.registerHandler(3, new etp.DiscoveryStore(ralf, store));
        }

        producer.simpleStreamer = argv.simpleStreamer;
        // Server-side, all messages go to console.
        ralf.on('log', function(msg){console.log(msg);});
        ralf.binary = request.httpRequest.headers['etp-encoding'] === 'binary';
        console.log((new Date()) + ': Connection accepted.');
        try {
            ralf.connection = request.accept(etp.WS_PROTOCOL, request.origin);
            ralf.start();
            g_Connections[ralf.sessionId] = ralf;
        }
        catch(e) {
            console.dir(e);
        }
    }
});

class PerfMonProducer extends etp.BaseHandler  {
    dataSimulator:any;
    nextChannelId:number = 0;
    simpleStreamer:boolean = false;

    constructor(private sessionManager: etp.ETPCore) {
        super(sessionManager);
        this._role = "producer";
        this._protocol = 1;
        this.dataSimulator = new perfmon.PerfMonProvider(sessionManager.sessionId, sessionManager.config);
        this.dataSimulator.on('metadata', this.onMetadata.bind(this));
        this.dataSimulator.on('data', this.onData.bind(this));
    }

    public start() {
        if (this.simpleStreamer) {
            this.dataSimulator.describe({header: {}, body: { uris: ["/"]}}, null, this.sessionManager.store);
            this.dataSimulator.startAllChannels();
        }
    }

    public stop() {
        if (this.dataSimulator)
            this.dataSimulator.stop();
        this.sessionManager = null;
    }

    public handleMessage(messageHeader, messageBody) {
        switch (messageHeader.messageType) {
            case 1 :
                if(!this.simpleStreamer) {
                    try {
                        this.log("Describe Request: " + messageBody.uris);
                        this.dataSimulator.describe({header: messageHeader, body: messageBody});
                    }
                    catch (exception) {
                        this.sessionManager.log(JSON.stringify(exception));
                        this.sessionManager.sendException(0, "Unable to describe URI: " + messageBody.uri, messageHeader.messageId);
                    }
                }
                break;
            case 9: // Range Request
                break;
            case 4 :
                if(!this.simpleStreamer)
                    this.dataSimulator.startStreaming(messageBody.channels);
                break;
            case 5 :
                if(!this.simpleStreamer)
                    this.dataSimulator.stopStreaming(messageBody.channels);
                break;
            default:
                super.handleMessage(messageHeader, messageBody);
                break;
        }
    }

    /**
     * Handle channel data from the provider, pass on to the consumer.
     * @param channelData
     */
    private onData(channelData) {
        var header = this.sessionManager.createHeader(1, 3, 0);
        this.sessionManager.send(header, channelData);
    }

    /** Handle new metadata coming from the provider */
    private onMetadata(channelMetadata, correlationId) {
        var header = this.sessionManager.createHeader(1, 2, correlationId);
        this.sessionManager.send(header, channelMetadata);
    }
}
