/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

const uuid = require('node-uuid');

import url = require('url');
import avro = require("etp-avro"); 
import { Energistics } from "etp";
import { ETPCore } from "../common/ETPCore";
import {IStore, NO_SESSION} from "../common/EnergisticsCommons";

const Core = Energistics.Protocol.Core;
const PROTOCOL = Energistics.Datatypes.Protocols;

/// Implements the client role for protocol 0
export class ETPClient extends ETPCore {
    host: string;

    constructor(config: any) {
        super(config);
    }

    serverProtocols: Energistics.Datatypes.SupportedProtocol[] = [];

    static atob (str) {
        return new Buffer(str, 'base64').toString('binary');
    }

    connect(config, socketClass) {
        
        if(!socketClass)
            socketClass = WebSocket;
        if (config.url != null)
            this.host = config.url;

        var encoding = config.encoding ? config.encoding : "binary";
        this.binary = (encoding === "binary");

        var headers = {
            "etp-encoding": encoding,
            "Authorization": ""
        };
        if (config.authentication=="basic") {
            headers.Authorization = 'Basic ' + new Buffer(config.username + ':' + config.password).toString('base64');
        }
        else if (config.authentication=="jwt") {
            headers.Authorization  = 'Bearer ' + config.token;
        }

        if(config.noHeaders) {
            this.host = this.host + "?etp-encoding=" + encoding;
            if (headers.Authorization) {
                this.host = this.host + "&Authorization=" + headers.Authorization;
            }
        }

        this.connection = new socketClass(
            this.host,
            "energistics-tp",   // protocol
            "localhost",        // origin
            headers             // http headers
            );
        if (this.binary)
            this.connection.binaryType = "arraybuffer";

        this.connection.onopen = (function (evt) {
            if (this.connection.readyState==1) {
                this.emit("connect", this.connection);
            }
        }).bind(this);

        this.connection.onclose = this.onSocketClose.bind(this);

        this.connection.onmessage = this.onSocketMessage.bind(this);

        this.connection.onerror = (function (evt) {
            this.emit('log', 'Connection Error');
        }).bind(this);
    }

    handleMessage(messageHeader, messageBody)
    {
        // this.emit('log', JSON.stringify({header: messageHeader, body: messageBody}));
        if(messageHeader.protocol == PROTOCOL.Core) {
            switch(messageHeader.messageType) {
                case Core.MsgOpenSession:
                    this.onOpenSession(messageHeader, messageBody);
                    break;
                case Core.MsgCloseSession:
                    this.onCloseSession(messageHeader, messageBody);
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            this.handlers[messageHeader.protocol].handleMessage(messageHeader, messageBody);
        }
    }

    onOpenSession(messageHeader, messageBody)
    {
        this.log("Opened Session {" + messageBody.sessionId + "} with " + this.host);
        this.sessionId = messageBody.sessionId;
        for (var i=0; i<messageBody.supportedProtocols.length; i++) {
            var prot = messageBody.supportedProtocols[i];
            var protocolId = prot.protocol;

            // Save it, so handlers can have access to any capabilities.
            this.serverProtocols[protocolId]=prot;
            this.log("Supported protocol: " + protocolId + "; " +prot.role+"; caps: "+JSON.stringify(prot.protocolCapabilities));
            if(this.handlers[protocolId]) {
                this.handlers[protocolId].start();
            }
        }
        this.emit('open');
    }

    onSocketClose(evt) {
        //console.dir(evt);
        this.sessionId = NO_SESSION;
        this.log((new Date()) + ' Peer ' + this.connection.url + ' disconnected.');
        this.log('--Reason Code [' + evt.code + '] Description [' + evt.reason + ']');
        this.emit('disconnect', this.connection);
    }

    onSocketMessage(msg) {
        var header = null;
        var message = null;
        this.stats.messagesReceived++;
        if (typeof (msg.data) == "object") {
            this.stats.bytesReceived += msg.data.byteLength;
            var reader = new avro.BinaryReader(this.schemaCache, new Uint8Array(msg.data));
            header = reader.readDatum("Energistics.Datatypes.MessageHeader");
            message = reader.readDatum(this.schemaCache.find(header.protocol, header.messageType));
        }
        else {
            this.stats.bytesReceived += msg.data.length;
            var data = JSON.parse(msg.data);
            header = data[0];
            message = data[1];
        }
        this.handleMessage(header, message);
    }

    requestSession(requestedProtocols, applicationName, applicationVersion = "") {
        //this.capabilities = caps;
        var header = this.createHeader(PROTOCOL.Core, Core.MsgRequestSession, 0);
        var message = {
            requestedProtocols: requestedProtocols,
            applicationName: applicationName,
            applicationVersion: applicationVersion
        };
        this.send(header, message);
    }

    send(header, message) {
        this.stats.messagesSent++;
        // this.log("send m: " +header.messageId +" p:"+ header.protocol+" t:"+header.messageType + " c:" + header.correlationId +" f:"+header.messageFlags);
        // this.emit('log', JSON.stringify({header: header, body: message}));

        var dataToSend = null, encoder;
        if (this.binary) {
            encoder = new avro.BinaryWriter(this.schemaCache);
            encoder.writeDatum("Energistics.Datatypes.MessageHeader", header);
            if (message) {
                var schemaName = this.schemaCache.find(header.protocol, header.messageType).fullName;
                encoder.writeDatum(schemaName, message);
            }
            dataToSend = encoder.getArrayBuffer();

        } else {
            dataToSend = JSON.stringify([header, message]);
        }

        this.stats.bytesSent += dataToSend.byteLength;
        this.connection.send(dataToSend);
        return header.messageId;
    }
}
