/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import url = require('url');

/// Turns 1411 into 1.4.1.1
function dottedVersion(str)
{
    var retVal = str[0];
    for(var i=1; i<str.length; i++) {
        retVal += ".";
        retVal += str[i];
    }
    return retVal;
}

/// Parse a uri for Etp
export class EtpUri {
    constructor(private _uri:string) {
        this._url = url.parse(_uri);
        this._emluri = this._uri.match(/^eml:\/\/(.*?)\/((witsml|resqml|prodml|energyml)([0-9]+))(\/((obj_)(\w+))(\(([\-\w]+)\))?)?$/i);
    }

    private _url:url.Url;
    private _emluri:any;
    type:string;
    id:string;

    eml(i:number):string {
        return (this._emluri) ? this._emluri[i] : "";
    }

    get isWitsml():boolean {
        return this.schemaFamily == "witsml";
    }

    get versionedFamily():string {
        return this.eml(2);
    }

    get schemaFamily():string {
        return this.eml(3);
    }

    get isUrn():boolean {
        return this._url.protocol == "urn:";
    }

    get isUuid():boolean {
        return this.isUrn && this._url.hostname == "uuid";
    }

    get isRoot():boolean {
        return this._uri == "/";
    }

    get hasDataspace():boolean {
        return this.eml(1) != "";
    }

    get objectType():string {
        return this.eml(6);
    }

    get collection():string {
        return this.eml(8);
    }

    get identifier():string {
        return this.eml(10);
    }

    get dataSpace():string {
        return this.eml(1);
    }

    get contentType():string {
        return "application/x-" + this.schemaFamily + "+xml;version=" + dottedVersion(this.eml(4)) + ";type=" + this.objectType;
    }
}
