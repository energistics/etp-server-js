/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */



import events = require("events");
import common = require('../common/EnergisticsCommons');

var Jsonix = require("jsonix").Jsonix;

/// Helper class for loading the data points in a Witsml Log
export class LogLoader {
    constructor(private store, private log: any, private parentLoader) {
        this.logData = log.logData;
        this.logCurveInfo = log.logCurveInfo;
        log.logData=null;
        log.logCurveInfo = null;
    }

    headerOnly: boolean = false;

    logData: any;

    logCurveInfo: any[];

    channelCount = 0;

    channelsRegistered = 0;

    /// Set this to false to parse the log as data frames instead of channel arrays.
    pivot: boolean = false;

    /// Map of mnemonic to logCurveInfo
    curveMap = {};

    /// array of channelMetadata records. This is essentially what gets loaded into to the channel table.
    channelMetadata = [];

    /// Holds the parsed log data organized by channel. Map of channelName to array of points.
    channelData = {};


    /// Holds the parsed log data organized by data frame. Array of array of dataPoints
    frameData = [];

    /// Map logCurveInfo data type to avro types.
    dataTypeFromCurveInfo(info)
    {
        return info.typeLogData;
    }

    channelDataMap() {
        var retVal = {};
        for(var i=0; i<this.channelMetadata.length; i++) {
            retVal[this.channelMetadata[i]._id]=this.channelMetadata[i];
        }
        return retVal;
    }

    /// Find the index track of the log and convert it to the one and only ETP IndexMetadata record.
    logToChannelIndex(log)
    {
        var indexMetadata:any={};
        var indexCurve;

        /// Find the index curve. I've come across a few where uids were missing.
        for(var i=0; i<this.logCurveInfo.length; i++){
            if (log.indexCurve == this.logCurveInfo[i].uid || log.indexCurve == this.logCurveInfo[i].mnemonic.value) {
                indexCurve = this.logCurveInfo[i];
                break;
            }
        }

        switch (log.indexType) {
            case "date time":
                indexMetadata.indexType = "Time";
                break;
            case "measured depth":
                indexMetadata.indexType = "Depth";
                break;
            default:
                indexMetadata = "Time";
        }

        /// ETP direction enumeration is proper case.
        indexMetadata.direction = log.direction.substr(0,1).toUpperCase() + log.direction.substr(1);

        /// WITSML 1411 wells dont have a datum on depth indexed logs.
        indexMetadata.datum = null;

        indexMetadata.uom = indexCurve.unit;

        indexMetadata.scale = 5;

        return indexMetadata;
    }

    /// Converts one logCurveInfo into ChannelMetadata
    curveInfoToChannelMetadata(log, info, theIndex)
    {
        return {
            channelUri: "eml:///witsml14/log(" + log.uid + ")/logCurveInfo(" + info.mnemonic.value + ")",
            contextUri: "eml:///witsml14/log(" + log.uid + ")",
            indexes: [theIndex],
            dataType: this.dataTypeFromCurveInfo(info),
            uom: info.unit,
            startIndex: null,
            endIndex: null,
            channelName: info.mnemonic.value,
            description: info.curveDescription,
            status: "Closed",
            source: log.serviceCompany,
            measureClass: "",

        };

    }

    /// Pivots the data frames into channel-oriented arrays of points.
    parseLogCurves() {
        // There could be multiple data blocks in the log. Treat each one separately, but
        // create one continuous array of points per-channel. Note that the channel value array is
        // not ordered.
        for (var dataSetId = 0; dataSetId<this.logData.length; dataSetId++) {

            var dataSet = this.logData[dataSetId];
            var mnemonicList = dataSet.mnemonicList.split(",");
            var unitList = dataSet.unitList.split(",");

            // The
            var indexInfo = this.curveMap[mnemonicList[1]].metadata.indexes[0];
            var rows = dataSet.data;

            if (rows && (rows.length>0)) {
                for (var rowNum = 0; rowNum < rows.length; rowNum++) {
                    var row = rows[rowNum].split(",");
                    var indexValue = row[0];
                    if (indexInfo.indexType == "Time") {
                        indexValue = common.encodeIndexValue(indexInfo,  Date.parse(indexValue));
                    }
                    else {
                        indexValue = common.encodeIndexValue(indexInfo,  indexValue);
                    }

                    if(this.pivot) {
                        for (var i = 1; i < row.length; i++) {
                            var channelMetadata = this.curveMap[mnemonicList[i]].metadata;
                            var dataArray = this.channelData[channelMetadata.channelName];
                            if (row[i] != "") {
                                var dataItem = {
                                    indexes: [indexValue],
                                    channelId: channelMetadata._id,
                                    value: {item: {double: parseFloat(row[i])}},
                                    valueAttributes: []
                                };
                                dataArray.push(dataItem);
                            }
                            else {
                            }
                        }

                    }
                    else {
                        var frame = [];
                        for (var i = 1; i < row.length; i++) {
                            var channelMetadata = this.curveMap[mnemonicList[i]].metadata;
                            var info = this.curveMap[mnemonicList[i]].info;
                            if (row[i] != "" && row[i]!=info.nullValue) {
                                var dataItem = {
                                    indexes: [],
                                    channelId: channelMetadata._id,
                                    //TODO - get proper value type
                                    value: {item: { double: parseFloat(row[i])} },
                                    valueAttributes: []
                                };
                                frame.push(dataItem);
                            }
                            else {
                                this.parentLoader.nullValues++;
                            }
                        }
                        if (frame.length>0) {
                            frame[0].indexes.push(indexValue);
                            this.frameData.push(frame);
                        }
                    }

                }
            }
        }
    }

    /// Inserts the parsed data into database
    insertLogCurves()
    {
        var done = 0;
        if (this.pivot) {
            for (var q = 0; q < this.channelMetadata.length; q++) {
                //console.log(channelMetadata[q].name);
                var channelInfo = this.channelMetadata[q];
                var curveName = channelInfo.channelName;
                var data = this.channelData[curveName];
                if (data && (data.length > 0)) {
                    this.store.putChannelValues(channelInfo, data, function () {
                        done++;
                        // console.log("Channel " + done);
                        if (done == this.channelMetadata.length) {
                            this.parentLoader.emit('done');
                        }
                    }.bind(this));

                }
                else {
                    this.parentLoader.emit('done');
                }
            }
        }
        else {
            this.store.putIndexAlignedValuesBatch(this.channelDataMap(), {bulk: true}, this.frameData, function(err, result){
                this.parentLoader.emit('done');
            }.bind(this));
        }
    }

    /// Loads log
    loadLog() {
        // Reset the map.
        this.curveMap = {};
        this.channelData = {};
        this.channelMetadata = [];

        if(!this.logCurveInfo) {
            this.parentLoader.emit('done');
            return;
        }

        var theIndex = this.logToChannelIndex(this.log);

        this.channelCount = this.logCurveInfo.length;
        this.channelsRegistered = 0;
        for (var i = 0; i < this.channelCount; i++) {

            var info = this.logCurveInfo[i];
            this.curveMap[info.mnemonic.value]={info: info};
            if(info.mnemonic.value != this.log.indexCurve) {
                var channelInfo :any = this.curveInfoToChannelMetadata(this.log, info, theIndex);
                this.channelMetadata.push(channelInfo);
                if(this.store) {
                    this.store.addChannel(channelInfo, function (err, data) {
                        this.channelsRegistered++;
                        // Index track doesnt get registered, so count is one less than number of logCurveInfos.
                        if (this.channelsRegistered == (this.channelCount - 1)) {

                            if (this.parentLoader.headerOnly==true) {
                                //console.log("Header only in logLog()");
                                this.parentLoader.emit("done");
                            }
                            else {
                                this.parseLogCurves();
                                //console.log("Loading curve info");
                                this.insertLogCurves();
                            }
                        }
                    }.bind(this));
                }
                else {
                    channelInfo.channelId = channelInfo._id = i;
                }
                this.curveMap[info.mnemonic.value].metadata = channelInfo;
                this.channelData[info.mnemonic.value]=[];

            }
        }
        if(!this.store) {
            this.parseLogCurves();
        }
    }
}

/// Helper class for loading the stations in a Witsml Trajectory
export class TrajectoryLoader {
    constructor(private store, private trajectory: any, private parentLoader){
        this.stations = this.trajectory.trajectoryStation;
        trajectory.trajectoryStation = null;
    }

    stations: any[];

    loadTrajectory() {
        this.parentLoader.emit("done");
    }
}

export class FileLoader  extends events.EventEmitter {

    constructor(private store, private config) {
        super();

        config.schemaModule || (config.schemaModule = "witsml1411");
    }

    headerOnly: boolean = false;

    /// This is the 'psuedo-message' for the store input. It simulates the data coming from
    /// the ETP store channel. It will have the fields header and body, plus 'nativeObject'
    /// for the parsed XML proxy object.
    messageData:any={};

    dataObject:any;
    log: LogLoader=null;
    trajectory: TrajectoryLoader=null;
    points: number = 0;
    rows: number = 0;
    channels: number = 0;
    stations: number = 0;
    nullValues: number=0;

    onLoadComplete() {
        this.emit("done");
    }

    start() {
        this.emit("ready");
    }

    handleLog(err, result) {
        this.dataObject.logData = this.messageData.logData;
        this.dataObject.logCurveInfo = this.messageData.logCurveInfo;
        this.log = new LogLoader(this.store, this.dataObject, this);
        this.log.headerOnly = this.headerOnly;
        // console.log(this.headerOnly);
        this.log.loadLog();
    }

    handleTrajectory(err, result) {
        this.trajectory = new TrajectoryLoader(this.store, this.dataObject, this);
        this.trajectory.loadTrajectory();
    }

    load_doc(file) {
        var schemas = require(this.config.schemaModule)[this.config.schemaModule];
        /// Serialization context for witsml schemas.
        var ctx = new Jsonix.Context([schemas]);
        /// Serializer
        var ser = ctx.createUnmarshaller();
        ser.unmarshalFile(file, function(doc) {

            // This will be the plural name, same as the collection name in mongo.
            var collectionName = doc.name.localPart;

            // Wack off the 's'
            var singularName = collectionName.substr(0, collectionName.length-1);

            // This will be a collection, but will normally have a length of 1
            var singularCollection = doc.value[singularName];
            for (var i=0; i<singularCollection.length; i++) {
                var callback=this.onLoadComplete.bind(this);
                this.dataObject = singularCollection[i];
                switch (singularName) {
                    case "log":
                        if(this.dataObject.logData
                            && this.dataObject.logData.length > 0
                            && this.dataObject.logData[0].data!=null) {
                            this.rows = this.dataObject.logData[0].data.length;
                            this.points = this.rows * this.dataObject.logCurveInfo.length;
                        }
                        if(this.dataObject.logCurveInfo)
                            this.channels = this.dataObject.logCurveInfo.length;
                        callback = this.handleLog.bind(this);
                        break;
                    case "trajectory":
                        if(this.dataObject.trajectoryStation) {
                            this.stations = this.dataObject.trajectoryStation.length;
                        }
                        callback = this.handleTrajectory.bind(this);
                        break;
                }
                if(this.store) {
                    this.messageData = {
                        header: {},
                        body: {
                            data : {
                                resource: {
                                    uri: "eml:///" + this.schemaModule + "/obj_"+singularName+"("+ this.dataObject.uid +")",
                                    contentType: "application/x-witsml+xml;version=1.4.1.1;type=obj_"+singularName
                                }
                            }
                        },
                        nativeObject: this.dataObject
                    };
                    if(this.config.dryRun!='true') {
                        this.store.put(this.messageData, callback);
                    }
                    else {
                        this.emit("done");
                    }
                }
                else {
                    callback(null, null);
                }
            }
        }.bind(this));

    }
}
