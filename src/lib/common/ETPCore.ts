/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import * as events from "events";
import {BaseHandler} from "./BaseHandler";
import {SchemaCache, Energistics} from "etp";
import {NO_SESSION, IStore} from "./EnergisticsCommons";

var Core = Energistics.Protocol.Core;
var PROTOCOL = Energistics.Datatypes.Protocols;

/// Base class for either a server or client, implements common core functionality
export class ETPCore extends events.EventEmitter {
    connection = null;
    name:string = "Energistics";
    schemas:any[];
    config:any = {};
    messageId:number = 0;
    schemaCache = new SchemaCache();
    sessionId:string = NO_SESSION;
    binary:boolean = true;
    handlers:BaseHandler[] = [];
    store:IStore = null;
    stats = {
        blocks: 0,
        rows: 0,
        dataPoints: 0,
        bytesSent: 0,
        bytesReceived: 0,
        messagesSent: 0,
        messagesReceived: 0,
        connectDate: new Date(),
        firstDataBlock: null,
        lastDataBlock: null
    };

    constructor(configuration, store:IStore = null) {
        super();
        //this.handlers[0] = this;
        this.config = configuration;
    }

    /// Send an acknowledgement of receipt of a message.
    acknowledge(messageId:number) {
        var header = this.createHeader(PROTOCOL.Core, Core.MsgAcknowledge, messageId, 0);
        var message = {};
        this.send(header, message);
    }

    /// Close the current Session. Other party should respond by closing the socket.
    closeSession() {
        if (this.sessionId) {
            this.send(this.createHeader(PROTOCOL.Core, Core.MsgCloseSession, 0, 0), {sessionId: this.sessionId});
            this.sessionId = NO_SESSION;
        }
    }

    /// Create a new header for a specific message kind and correlation id
    createHeader(protocol, messageType, correlationId = 0, messageFlags = 0 ) {
        return {
            protocol: protocol,
            messageType: messageType,
            messageId: ++this.messageId,
            correlationId: correlationId,
            messageFlags: messageFlags
        };
    }

    /// Are we in an active session.
    isInSession() {
        return this.connection != null
            && this.connection.readyState == 1
            && this.sessionId != NO_SESSION;
    }

    /// Handle a parsed message. Overloaded in all handlers.  Includes default handling for common messages
    handleMessage(messageHeader, messageBody) {
        switch (messageHeader.messageType) {
            case Core.MsgAcknowledge:
                this.onAcknowledge(messageHeader, messageBody);
                break;
            case Core.MsgProtocolException:
                this.onException(messageHeader, messageBody);
                break;
        }
    }

    /// Log a message
    log(message:string) {
        this.emit('log', message);
    }

    /// Handle the close session message. This can come from server or client on protocol 0
    onCloseSession(header, message) {
        var i;
        for (i = 0; i < this.handlers.length; i++) {
            if (this.handlers[i])
                this.handlers[i].stop();
        }
        this.log("Received CloseSession message for session {" + this.sessionId + "} Closing WebSocket.");
        this.sessionId = NO_SESSION;
        this.connection.close();
        this.emit('close');
    }

    /// Handle an acknowledgement model.
    onAcknowledge(header, message) {
        this.emit('acknowledge', header, message);
    }

    /// Handle an exception message.
    onException(header, message) {
        this.log("EXCEPTION: " + message.errorMessage);
        this.emit("exception", header, message);
    }

    /// Register a new handler for a protocol
    registerHandler(id:number, handler:BaseHandler) {
        this.handlers[id] = handler;
        return handler;
    }

    /// No body, so client and server can fill in their own way
    send(header, message) {
    }

    /// Send an exception, either direction
    sendException(errorCode:number, errorMessage:string, correlationId:number ) {
        var header = this.createHeader(PROTOCOL.Core, Core.MsgProtocolException, correlationId, 0);
        var message = {errorCode: errorCode, errorMessage: errorMessage};
        this.send(header, message);
    }

}

