/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import * as events from "events";
import * as etp from "etp";

/// The websocket subprotocol name for etp
export const WS_PROTOCOL = "energistics-tp";

export const NO_SESSION:string = "";

export var EINVALID_MESSAGE_TYPE = 3;

/// The interface that must be implemented to be a permanent store of channel data
export interface IChannelStore extends events.EventEmitter {
    addChannel(channelInfo:etp.Energistics.Datatypes.ChannelData.ChannelMetadataRecord, callback:(err, data) => void);
    putChannelValue(channel_id:string, value:etp.Energistics.Datatypes.ChannelData.DataItem, callback:(err, data) => void);
    describeChannels(uris:string[], callback:(err, data) => void);
    getChannelRange(channelInfo:any, startIndex:number, endIndex:number, callback);
}

/// The interface that must be implemented for the store protocols.
export interface IStore extends events.EventEmitter {
    connect(connectionString:string);
    delete(uri:string);
    enum(uri:string, handler:(err, data) => void);
    get(uri:string, handler:(err, data) => void);
    notificationRequest(messageData, callback:()=>void);
    put(messageData:any, handler:(err, data) => void);
}

export interface IStoreNotification {
    subscribe(request:etp.Energistics.Protocol.StoreNotification.NotificationRequest);
}

/// Composite interface implementing a store for all protocols
export interface ISuperStore extends IChannelStore, IStore, events.EventEmitter {

}

export interface ILogger {
    log(str:string);
}

/// Convert string 2 byte array
export function String2Buffer(str) {
    var buf = new ArrayBuffer(str.length);
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return bufView;
}

export function uriFromNative(object:any) {
    var retVal:etp.Energistics.Datatypes.Object.Resource = new etp.Energistics.Datatypes.Object.Resource();
    if (object.TYPE_NAME) {
        var pattern = object.TYPE_NAME.match(/^([\w+]ml)(\d+)\.Obj(\w+)$/i);
    }
    return null;
}


export function etpDate(dateIn:Date = null) {
    dateIn || (dateIn = new Date());
    return dateIn.valueOf() * 1000;
}

export function encodeIndexValue(metadata:any, value:number):number {
    if (!value)
        return null;
    switch (metadata.indexType) {
        case "Time":
        case "ElapsedTime":
            return value * 1000;
        default:
            return value * Math.pow(10, metadata.scale);
    }
}

export function decodeIndexValue(metadata:any, value:number):any {
    if (!value)
        return null;
    switch (metadata.indexType || metadata) {
        case "Time":
        case "ElapsedTime":
            return new Date(value / 1000);
        default:
            return value / Math.pow(10, metadata.scale);
    }
}
