/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */


var Jsonix = require('jsonix').Jsonix;
import etp = require("etp");
import common = require("./EnergisticsCommons");
import { EtpContentType } from "./EtpContentType";
/// Converts a version string all clumped together (like 1411) to 1.4.1.1
function dottedVersion(str)
{
    var retVal = str[0];
    for(var i=1; i<str.length; i++) {
        retVal += ".";
        retVal += str[i];
    }
    return retVal;
}

var schemas = {
    "witsml1411": require("witsml1411"),
    "witsml20": require("witsml20"),
    "resqml20": require("resqml20"),
    "prodml122": require("prodml122")
};

/// Handle marshalling/un-marshalling of Xml based on content-type
export class DataObjectFactory {
    constructor(context: any)
    {
        if(context.hasOwnProperty("TYPE_NAME")) {
            this.fromNative(context);
        }
        if (typeof context == "string") {

            this.contentType = new EtpContentType(context);
            if(this.contentType.valid) {
                this._packageName = this.contentType.schemaFamily + this.contentType.version.replace(/[v\.]/ig, "");
                this._needsPlural = this.contentType.hasPlural;
            }
            else {
                this._packageName = context;
            }
        }
    }
    private _packageName: string;
    private _needsPlural: boolean;
    public contentType: EtpContentType=null;


    getPackage():any
    {
        var module=schemas[this._packageName];
        return module[this._packageName];
    }
    get packageName()
    {
        return this._packageName;
    }

    private getContext()
    {
        return new Jsonix.Context([this.getPackage()]);
    }

    deserializeString(xmlString: string) {
        var context = this.getContext();
        var deserializer = context.createUnmarshaller();
        return deserializer.unmarshalString(xmlString);
    }

    deserializeFile(fileName: string, callback: (object) => void) {
        var context = this.getContext();
        var deserializer = context.createUnmarshaller();
        deserializer.unmarshalFile(fileName, callback);
    }

    serializeString(object: any) : string {
        var ct = this.contentType;
        var context = this.getContext();
        var ser = context.createMarshaller();
        if(this._needsPlural) {
            var plural = {
                name: {
                    localPart: ct.pluralName,
                    prefix:context.modules[0].name[0],
                    namespaceURI:  context.modules[0].targetNamespace
                },
                value: {
                    version: ct.version,
                }
            };
            plural.value[ct.singularName]=[object];
            return ser.marshalString(plural);
        }
        else {

        }
        //delete object._id;
    }

    private fromNative(native: any)
    {
        this.contentType = new EtpContentType(DataObjectFactory.contentTypeFromNative(native));
        this._packageName = this.contentType.schemaFamily + this.contentType.version.replace(/[v\.]/ig, "");
        this._needsPlural = this.contentType.hasPlural;
    }

    public static contentTypeFromNative(native: any) : string {
        var pattern = native.TYPE_NAME.match(/^(\w+ml)(\d+)\.Obj(\w+)$/i);
        return "application/x-"+pattern[1]+"+xml;version=" + dottedVersion(pattern[2])+";type=obj_"+pattern[3].toLowerCase();
    }

    public static resourceFromNative(native: any) : etp.Energistics.Datatypes.Object.Resource {
        var ct = new EtpContentType( DataObjectFactory.contentTypeFromNative(native) );
        var retVal: etp.Energistics.Datatypes.Object.Resource = new etp.Energistics.Datatypes.Object.Resource();
        retVal.uri = ct.dataType + "(" + native._id + ")";
        retVal.name = native.name;
        retVal.resourceType = "DataObject";
        retVal.objectNotifiable = true;
        retVal.hasChildren = -1;
        retVal.channelSubscribable = (["well", "wellbore", "channel", "channelSet", "log"].indexOf(ct.dataType) >= 0);
        retVal.contentType = ct.contentType;
        if(native.commonData && native.commonData.dTimLastChanged) {
            retVal.lastChanged = common.etpDate(native.commonData.dTimLastChanged.date);
        }
        return retVal;
    }

    public static dataObjectFromNative(native: any) : any {
        var retVal: any = {};
        retVal.resource = DataObjectFactory.resourceFromNative(native);
        retVal.contentEncoding = "text/xml";
        var xmlFactory = new DataObjectFactory(retVal.resource.contentType);
        retVal.data = common.String2Buffer(xmlFactory.serializeString(native));
        return retVal;
    }
}
