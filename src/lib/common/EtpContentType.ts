/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

/// parse a standard content type for an energistics data object.
export class EtpContentType {
    constructor(private _contentType: string){
        this._m = this._contentType.match(/^application\/x-(witsml|resqml|prodml)\+xml;version=([0-9.]+);type=(obj_\w+)$/i);
    }

    private _m: any;

    // All of the versions prior to 2.0 have the plural root, except Resqml.
    get hasPlural() : boolean {
        return ((this._m[2][0] == "1") && this._m[1]!="resqml");
    }

    get schemaFamily() : string {
        return this._m[1];
    }

    get version() : string {
        return this._m[2];
    }

    get dataType() : string {
        return this._m[3];
    }

    get pluralName(): string {
        return this._m[3].substr(4)+"s";
    }

    get singularName(): string {
        return this._m[3].substr(4);
    }

    get valid() : boolean {
        return this._m != null;
    }

    get contentType() : string {
        return this._contentType;
    }
}
