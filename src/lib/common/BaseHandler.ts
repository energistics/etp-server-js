/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import * as events from "events";
import {ILogger} from "./EnergisticsCommons";

/// 'abstract' Base class for all protocol handlers other than protocol 0
export class BaseHandler extends events.EventEmitter  {
    _role: string = "";
    _protocol: number = -1;
    constructor(private logger: ILogger) {
        super();
    }
    get role(): string { return this._role; }
    get protocol(): number { return this._protocol; }
    start(){}
    stop(){}
    handleMessage(messageHeader, messageBody)
    {
        switch (messageHeader.messageType) {
            case 0:
                console.log("Received protocol 1 start message.");
                this.start();
                break;
            case 1000:
                this.logger.log(messageBody);
                break;
            case 1001:
                this.logger.log("Received Acknowledge.")
                break;
            default :
                throw ("Unsupported message {" + messageHeader.messageType + "}");
        }

    }

    log(str){
        this.logger.log(str);
    }
}