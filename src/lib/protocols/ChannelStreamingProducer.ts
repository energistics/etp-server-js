/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import {Energistics} from "etp";
import {BaseHandler} from "../common/BaseHandler";
import {ETPCore} from "../common/ETPCore";

var ChannelStreaming = Energistics.Protocol.ChannelStreaming;
var PROTOCOL = Energistics.Datatypes.Protocols;

/***
 * Implements the producer end of protocol 1
 */
export class ChannelStreamingProducer extends BaseHandler {
    nextChannelId:number = 0;
    simpleStreamer:boolean = false;

    constructor(public sessionManager:ETPCore, private dataSource: any) {
        super(sessionManager);
        this._role = "producer";
        this._protocol = 1;
        this.simpleStreamer = sessionManager.config.simpleStreamer;
    }

    start() {
        this.dataSource.on('metadata', this.onMetadata.bind(this));
        this.dataSource.on('data', this.onData.bind(this));

        // No support for control messages, so just start streaming
        if (this.simpleStreamer) {
            this.dataSource.describe({header: {correlationId: 0}, body: { uris: ["/"]}});
            this.dataSource.channelStreamingStart([]);
        }
    }

    stop() {
        if (this.dataSource)
            this.dataSource.stop();
        this.sessionManager = null;
    }

    handleMessage(messageHeader, messageBody) {
        switch (messageHeader.messageType) {
            case 0:
                this.start();
                break;
            case ChannelStreaming.MsgChannelDescribe :
                try {
                    this.dataSource.describe({header: messageHeader, body: messageBody});
                }
                catch (exception) {
                    this.sessionManager.log(JSON.stringify(exception));
                    this.sessionManager.sendException(0, "Unable to describe URI " + messageBody.uris, messageHeader.messageId);
                }
                break;
            case ChannelStreaming.MsgChannelRangeRequest:
                this.dataSource.rangeRequest({header: messageHeader, body: messageBody});
                break;
            case ChannelStreaming.MsgChannelStreamingStart :
                this.dataSource.startStreaming(messageBody.channels);
                break;
            case ChannelStreaming.MsgChannelStreamingStop :
                this.dataSource.stopStreaming(messageBody.channels);
                break;
            default:
                super.handleMessage(messageHeader, messageBody);
                break;
        }
    }

    /**
     * Handle channel data from the provider, pass on to the consumer.
     * @param channelData
     */
    onData(channelData, correlationId) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelStreaming, ChannelStreaming.MsgChannelData, correlationId);
        this.sessionManager.send(header, channelData);
    }

    /** Handle new metadata coming from the provider */
    onMetadata(channelMetadata, correlationId) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelStreaming, ChannelStreaming.MsgChannelMetadata, correlationId);
        this.sessionManager.send(header, channelMetadata);
    }

    /** Generate a unique channel ID for this session */
    newChannelId():number {
        return this.nextChannelId++;
    }
}

