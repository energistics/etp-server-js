/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import {Energistics} from "etp";
import {BaseHandler} from "../common/BaseHandler";
import {ETPCore} from "../common/ETPCore";

var Discovery = Energistics.Protocol.Discovery;
var PROTOCOL = Energistics.Datatypes.Protocols;

/// Implements the customer end of protocol 3
export class DiscoveryCustomer extends BaseHandler {
    constructor(public sessionManager: ETPCore) {
        super(sessionManager);
        this._role = "customer";
        this._protocol = PROTOCOL.Discovery;
    }

    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Discovery) {
            switch(messageHeader.messageType) {
                case Discovery.MsgGetResourcesResponse :
                    this.emit("resource",messageHeader, messageBody);
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in DiscoveryCustomer")
        }
    }

    get(uri: string): Number {
        var header = this.sessionManager.createHeader(PROTOCOL.Discovery, Discovery.MsgGetResources, 0);
        this.sessionManager.send(header, {uri: uri});
        return header.messageId;
    }
}

