/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import {Energistics} from "etp";
import {BaseHandler} from "../common/BaseHandler";
import {ETPCore} from "../common/ETPCore";
import {IStore, String2Buffer} from "../common/EnergisticsCommons";

var StoreNotification = Energistics.Protocol.StoreNotification;
var PROTOCOL = Energistics.Datatypes.Protocols;

/// Determine if a given change message is valid in the context of one of our subscriptions
function changeMatchesContext(change, context) {
    return (context.body.request.uri=="/")
        && ( ( context.body.request.objectTypes.indexOf(change.dataObject.resource.contentType ) >=0 )
            || context.body.request.objectTypes.length==0 );
}

/// Send a change notification to customer if it matches on of our subscriptions.
function sendChangeIfMatches(request) {
    if(changeMatchesContext(this.change, request)) {
        this.handler.log("Sending " + this.change.changeType + "notification");
        var header, body;
        this.change.dataObject.data = String2Buffer("");
        if(this.change.changeType=="Delete") {
            header = this.handler.sessionManager.createHeader(PROTOCOL.StoreNotification, StoreNotification.MsgDeleteNotification, request.header.messageId);
            body = { delete: this.change };
        }
        else {
            header = this.handler.sessionManager.createHeader(PROTOCOL.StoreNotification, StoreNotification.MsgChangeNotification, request.header.messageId);
            body = { change: this.change };

        }
        this.handler.sessionManager.send(header, body);
    }
}

/// Implements the store end of protocol 5
export class StoreNotificationStore extends BaseHandler {

    requests: any[] = [];

    constructor(public sessionManager: ETPCore, public store: IStore) {
        super(sessionManager);
        this.store = store;
        this._role = "store";
        this._protocol = PROTOCOL.StoreNotification;
    }

    /// Handle protocol messages.
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.StoreNotification) {
            switch(messageHeader.messageType) {
                case StoreNotification.MsgNotificationRequest :
                    this.log("Received notification subscription: " + JSON.stringify(messageBody));
                    this.requests.push({header: messageHeader, body: messageBody});
                    this.store.notificationRequest({header: messageHeader, body: messageBody}, this.onChangeNotification.bind(this));
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in StoreNotification")
        }
    }

    /// Handle change notifications from the store.
    onChangeNotification(change) {
        this.requests.forEach(sendChangeIfMatches, {handler: this, change: change});
    }
}
