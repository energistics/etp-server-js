/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import {Energistics} from "etp";
import {BaseHandler} from "../common/BaseHandler";
import {ETPCore} from "../common/ETPCore";
import {IStore} from "../common/EnergisticsCommons";

var Discovery = Energistics.Protocol.Discovery;
var PROTOCOL = Energistics.Datatypes.Protocols;

/// Store callback for GetResources
function resource_callback(err, data) {
    if(err) {
        this.handler.log(err);
    }
    else if(data[0]) {
        // console.log(data);
        var resources;
        if (data[0].logCurveInfo) {
            resources = data[0].logCurveInfo.map(function(x){ return {name: x.mnemonic, uri: this._id + "/obj_channel(" + x.mnemonic + ")"}}, data[0]);
        }
        else {
            resources = data;
        }
        try {

            resources.forEach(function(resource, i, src){
                console.log(i + " of " + src.length + " messages");
                resource.uri = resource.uri.replace("____", resource.uid);
                var header = this.handler.sessionManager.createHeader(PROTOCOL.Discovery, Discovery.MsgGetResourcesResponse, this.header.messageId);
                if(src.length==1)
                    header.messageFlags = 0;
                else if (i==src.length-1)
                    header.messageFlags = 3;
                else
                    header.messageFlags = 1;
                console.log(header.messageFlags);
                this.handler.sessionManager.send(header, {resource: resource});
            }.bind(this));
        }
        catch (e) {
            this.handler.log(e);
        }
    }
}

/// Implements store end of protocol 3
export class DiscoveryStore extends BaseHandler {
    store: IStore = null;
    constructor(public sessionManager: ETPCore, store: IStore) {
        super(sessionManager);
        this.store = store;
        this._role = "store";
        this._protocol = 3;
    }
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Discovery) {
            switch(messageHeader.messageType) {
                case Discovery.MsgGetResources :
                    messageBody.uri = decodeURI(messageBody.uri);
                    this.log("Discovery: Get " + messageBody.uri);
                    if (this.store)
                        this.store.enum(messageBody.uri, resource_callback.bind({handler: this, header: messageHeader, body: messageBody}));
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in DiscoveryStore")
        }
    }
}
