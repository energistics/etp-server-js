/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import {Energistics} from "etp";
import {BaseHandler} from "../common/BaseHandler";
import {ETPCore} from "../common/ETPCore";

var ChannelStreaming = Energistics.Protocol.ChannelStreaming;
var PROTOCOL = Energistics.Datatypes.Protocols;

/**
 * Implements the consumer end of protocol 1
 */
export class ChannelStreamingConsumer extends BaseHandler {
    constructor(public sessionManager: ETPCore) {
        super(sessionManager);
        this._role = "consumer";
        this._protocol = 1;
    }

    start() {
        super.start();
        this.sessionManager.log("Starting new ChannelStreamingConsumer.");
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelStreaming, 0 /* Start is always 0 */, 0 /*No correlation ID */);
        var message = {role: "ChannelStreamingConsumer", maxMessageRate: 0, maxDataItems: 10};
        this.sessionManager.send(header, message);
    }

    describe(uris:string[]) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelStreaming, ChannelStreaming.MsgChannelDescribe, 0);
        this.sessionManager.send(header, {uris: uris});
    }

    handleMessage(messageHeader, messageBody) {
        switch (messageHeader.messageType) {
            case ChannelStreaming.MsgChannelMetadata:
                this.emit('metadata', {header: messageHeader, body : messageBody});
                break;
            case ChannelStreaming.MsgChannelData:
                this.emit('data', {header: messageHeader, body: messageBody});
                break;
            default:
                super.handleMessage(messageHeader, messageBody);
                break;
        }
    }

    channelRangeRequest(channelRanges: Energistics.Datatypes.ChannelData.ChannelRangeInfo[]) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelStreaming, ChannelStreaming.MsgChannelRangeRequest, 0);
        return this.sessionManager.send(header, {channelRanges: channelRanges});
    }

    channelStreamingStart(channelIds: Energistics.Datatypes.ChannelData.ChannelStreamingInfo[]) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelStreaming, ChannelStreaming.MsgChannelStreamingStart, 0);
        return this.sessionManager.send(header, {channels: channelIds});
    }

    channelStreamingStop(channelIds: number[]) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelStreaming, ChannelStreaming.MsgChannelStreamingStop, 0);
        return this.sessionManager.send(header, {channels: channelIds});
    }
}
