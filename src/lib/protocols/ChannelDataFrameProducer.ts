/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import {Energistics} from "etp";
import {BaseHandler} from "../common/BaseHandler";
import {ETPCore} from "../common/ETPCore";

var ChannelDataFrame = Energistics.Protocol.ChannelDataFrame;
var PROTOCOL = Energistics.Datatypes.Protocols;

///Implements the producer end of protocol 2
export class ChannelDataFrameProducer extends BaseHandler {
    constructor(public sessionManager: ETPCore) {
        super(sessionManager);
        this._role = "producer";
        this._protocol = 2;
    }

    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.ChannelDataFrame) {
            switch(messageHeader.messageType) {
                case ChannelDataFrame.MsgRequestChannelData :
                    try {
                        //this.dataSource.realTime = false;
                        //this.dataSource.describe(messageBody.uri, this.newChannelId.bind(this), this.sessionManager.store);
                    }
                    catch(exception) {
                        this.log(JSON.stringify(exception));
                        this.sessionManager.sendException(0, "Unable to describe URI: " + messageBody.uri, messageHeader.messageId);
                    }
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
                    break;
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in HistoricalProducer");
        }
    }

    /**
     * Handle channel data from the provider, pass on to the consumer.
     * @param channelData
     */
    onData(channelData) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelDataFrame, ChannelDataFrame.MsgChannelDataFrameSet, 0, 0);
        this.sessionManager.send(header, channelData);
    }

    onMetadata(channelMetadata) {
        var header = this.sessionManager.createHeader(PROTOCOL.ChannelDataFrame, ChannelDataFrame.MsgChannelMetadata, 0);
        this.sessionManager.send(header, channelMetadata);
    }
}
