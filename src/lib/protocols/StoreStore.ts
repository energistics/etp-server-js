/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import {Energistics} from "etp";
import {BaseHandler} from "../common/BaseHandler";
import {ETPCore} from "../common/ETPCore";
import {IStore} from "../common/EnergisticsCommons";

var Store = Energistics.Protocol.Store;
var PROTOCOL = Energistics.Datatypes.Protocols;

/**
 * Store callback for GetObject. Data is an array, should only have one object.
 */
function object_callback(err, data) {
    if(err) {
        this.handler.log(err);
    }
    else if (data)  {
        var header = this.handler.sessionManager.createHeader(PROTOCOL.Store, Store.MsgObject, this.header.messageId);
        this.handler.sessionManager.send(header, data);
    }
    else {
        this.handler.sessionManager.sendException(-1 /*TODO*/, "Uri ["+this.body.uri+"] not found", this.header.messageId);
    }
}

/**
 * Implements the server side of protocol 4
 * */
export class StoreStore extends BaseHandler {
    constructor(public sessionManager: ETPCore, public store: IStore) {
        super(sessionManager);
        this.store = store;
        this._role = "store";
        this._protocol = PROTOCOL.Store;
    }
    handleMessage(messageHeader, messageBody)
    {
        if(messageHeader.protocol == PROTOCOL.Store) {
            switch(messageHeader.messageType) {
                case Store.MsgGetObject :
                    this.log("Get " + messageBody.uri + " from store.");
                    this.store.get(messageBody.uri, object_callback.bind({handler: this, header: messageHeader, body: messageBody}));
                    break;
                case Store.MsgPutObject :
                    this.store.put({header:messageHeader, body: messageBody}, function(err, result){});
                    break;
                case Store.MsgDeleteObject :
                    messageBody.uri.forEach(this.store.delete, this.store);
                    break;
                default:
                    super.handleMessage(messageHeader, messageBody);
            }
        }
        else {
            throw("Unsupported protocol {" + messageHeader.protocol + "} in StoreStore")
        }
    }
}
