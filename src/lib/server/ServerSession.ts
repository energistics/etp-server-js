/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import path = require("path");
import fs = require("fs");
import url = require('url');
import avro = require("etp-avro");
const uuid = require('node-uuid');

import { Energistics } from "etp";
import { ETPCore } from "../common/ETPCore";
import { IStore } from "../common/EnergisticsCommons";

const Core = Energistics.Protocol.Core;
const PROTOCOL = Energistics.Datatypes.Protocols;
const appRoot = path.dirname(path.dirname(path.dirname(module.filename)));

/// Implements the server role of protocol 0
export class ServerSession extends ETPCore {
    constructor(configuration, store: IStore = null) {
        super( configuration, store );
        this.sessionId = uuid.v4();
    }

    loggedMessageId: number = 0;
    start() {
        console.log("Starting ServerSession: ");
        if(this.config.traceMessages) {
            fs.mkdirSync(appRoot + '/trace/' + this.sessionId);
        }
        console.dir(this.config);
        console.log("Binary: ", this.binary);
        this.connection.on('message', this.onSocketMessage.bind(this));
        this.connection.on('close', this.onSocketClose.bind(this));
    }

    handleMessage(messageHeader, messageBody)
    {
        if(this.config.traceMessages) {
            var msgJson = JSON.stringify(["R", messageHeader, messageBody], null, 4);
            fs.writeFile(appRoot + "/trace/"  + this.sessionId + "/" + (++this.loggedMessageId) + ".json", msgJson);
        }
        try {
            if (messageHeader.protocol == PROTOCOL.Core) {
                switch (messageHeader.messageType) {
                    case Core.MsgRequestSession:
                        return this.onRequestSession(messageHeader, messageBody);
                    case Core.MsgCloseSession:
                        return this.onCloseSession(messageHeader, messageBody);

                }
                return super.handleMessage(messageHeader, messageBody);
            }
            else {
                if (this.handlers[messageHeader.protocol])
                    this.handlers[messageHeader.protocol].handleMessage(messageHeader, messageBody);
                else
                    this.sendException(-1, "Unsupported protocol", messageHeader.messageId);
            }
        }
        catch(err) {
            this.log(err);
        }
    }

    onRequestSession(header, body) {
        var agreedProtocols=[];
        for(var i=0; i<body.requestedProtocols.length; i++) {
            var protocol = body.requestedProtocols[i];
            if (this.handlers[protocol.protocol] && this.handlers[protocol.protocol].role==protocol.role) {
                agreedProtocols.push(protocol)
            }
        }
        if (this.config.simpleStreamer && agreedProtocols[0].protocol==1) {
            agreedProtocols[0].protocolCapabilities = { simpleStreamer: { item:  { boolean: true } } };
        }
        var msg = {
            applicationName: "ralf-server",
            applicationVersion: "0.0.1",
            sessionId: (this.sessionId),
            supportedProtocols: agreedProtocols
        };

        if (msg.supportedProtocols.length > 0) {
            this.log("Opening new session {" + msg.sessionId + "} with " + body.applicationName);
            this.send(this.createHeader(PROTOCOL.Core, Core.MsgOpenSession, header.messageId), msg);
        }
        else {
            this.log("No supported protocols: " + JSON.stringify(body));
            var error = {errorCode:2, errorMessage:"No supported protocols."};
            this.send(this.createHeader(PROTOCOL.Core, Core.MsgProtocolException, header.messageId), error);
        }
        for(var i=0; i<agreedProtocols.length; i++) {
            var protocol = agreedProtocols[i];
            this.log("Using protocol: " + protocol.protocol + " in role of '" + protocol.role + "'; + with caps: " + JSON.stringify(protocol.protocolCapabilities));
            //if (this.handlers[protocol.protocol].role==protocol.role) {
            // this.handlers[protocol.protocol].start();
            //}
        }
    }

    onSocketClose(reasonCode, description) {
        this.log((new Date()) + ' Peer ' + this.connection.remoteAddress + ' disconnected.');
        this.log('--Reason Code [' + reasonCode + '] Description [' + description + ']');
        this.emit('disconnect');
        for(var i=0; i<this.handlers.length; i++) {
            if(this.handlers[i])
                this.handlers[i].stop();
        }
        /// For single session servers, mostly for testing.
        if (this.config.exitOnClose) {
            this.log("Exiting process normally because started with --exitOnClose.");
            process.exit(0);
        }
    }

    onSocketMessage(message, flags) {
        this.stats.messagesReceived++;
        if (message.type === 'utf8' || (flags && (!flags.binary))) {
            this.binary = false;
            this.stats.bytesReceived += message.utf8Data.length;
            var parts = JSON.parse(message.utf8Data);
            this.handleMessage(parts[0], parts[1]);
        } else {
            this.stats.bytesReceived += message.binaryData.length;
            var reader = new avro.BinaryReader(this.schemaCache, new Uint8Array(message.binaryData)),
                header = reader.readDatum("Energistics.Datatypes.MessageHeader"),
                body = reader.readDatum(this.schemaCache.find(header.protocol, header.messageType));
            this.handleMessage(header, body);
        }
    }

    resume(){
    }

    send(header, message) {
        var dataToSend = null,
            encoder = null,
            schemaName = this.schemaCache.find(header.protocol, header.messageType).fullName;
        if(this.config.traceMessages) {
            var msgJson = JSON.stringify(["S", header, message], null, 4);
            fs.writeFile(appRoot + "/trace/"  + this.sessionId + "/" + (++this.loggedMessageId) + ".json", msgJson);
        }
        this.stats.messagesSent++;

        if (this.binary) {
            encoder = new avro.BinaryWriter(this.schemaCache);
            encoder.writeDatum("Energistics.Datatypes.MessageHeader", header);
            encoder.writeDatum(schemaName, message);
            dataToSend = encoder.getBuffer();

        } else {
            dataToSend = JSON.stringify([header, message]);
        }

        this.stats.bytesSent += dataToSend.length;
        this.connection.send(dataToSend, { binary: this.binary, mask: false });
    }
}