/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import * as etp from "./Energistics"
import express = require("express");
import {Request, Response} from 'express';
import {json, ParsedAsText} from 'body-parser';
import optimist = require("optimist");
import jwt = require("jsonwebtoken");
import fs= require("fs");
import http = require("http");
import https = require("https");
import ws = require("websocket");
import {VerifyOptions} from "jsonwebtoken";

class EtpServer {

    private app = express();    
	private server;
    private recorders: any[]=[];
    private connections: etp.ServerSession[]=[];
    private storeClass;
    private providerClass;
    private consumerClass;
    private store: etp.IStore = null;
    private config: any;

    constructor() {

        this.app.get('/', function (req, res) {
            res.send('Hello World!');
        });
		
		this.app.post('/api/*', (req, res, next) => {
            console.log('got api request');
            json()(req, res, next);
		});
		
		this.app.use(express.static('public'));

        this.app.use(function (req: Request,res: Response, next){
            console.log(new Date(), "Express:", req.method, req.url);
            next();
        });

    }
	
	use(pattern, handler: (req: Request&ParsedAsText, res: Response ) => any) {
		this.app.use(pattern, handler);
		return null;
	}

    authenticate(request) {
        switch (this.config.autentication) {
            case 'jwt':
                break;
            case 'basic':
                break;
        }
        if(this.config.authentication=="jwt") {
            try {
                var token = request.httpRequest.headers['authorization'];
                // The JWT will follow the word "Bearer " in the Authorization Header, so take substr(7)
                var decoded = jwt.verify(token.substr(7), this.config.jwt.signingSecret, <VerifyOptions>{alg: ['HS256']});

                console.dir(decoded);
                return true;
            }
            catch (e) {
                console.dir(e);
                return false;
            }
        }
        else if (this.config.authentication=="basic") {

        }
        else {
            return true;
        }

        return false;
    }

    createConnection(request)
    {
        let ralf = new etp.ServerSession(this.config, this.store);
        let realTimeDataSource = new this.providerClass(this.config, this.store);
        realTimeDataSource.store = this.store;
        let producer: any = ralf.registerHandler(1, new etp.ChannelStreamingProducer(ralf, realTimeDataSource));
        producer.simpleProducer = this.config.simpleProducer;
        // simpleProducer causes us to behave like a dumb server, supporting only
        // protocols 0 and part of 1
        if (!this.config.simpleProducer) {
            //ralf.registerHandler(2, new etp.ChannelDataFrameProducer(ralf, realTimeDataSource));
            ralf.registerHandler(3, new etp.DiscoveryStore(ralf, this.store));
            ralf.registerHandler(4, new etp.StoreStore(ralf, this.store));
            ralf.registerHandler(5, new etp.StoreNotificationStore(ralf, this.store));
        }

        // Server-side, all messages go to console.
        ralf.on('log', function(msg){console.log(msg);});

        ralf.binary = request.httpRequest.headers['etp-encoding'] === 'binary';
        console.log((new Date()) + ': Connection accepted.');
        console.log(request.httpRequest.url);
        ralf.connection = request.accept(etp.WS_PROTOCOL, request.origin);
        ralf.start();
        this.connections[ralf.sessionId]=ralf;
    }

    createServer() {
        if (this.config.useTls) {
            var sslConfig;
            if(this.config.pfxFile!='') {
                sslConfig= {
                    pfx: fs.readFileSync(this.config.pfxFile),
                    passphrase: this.config.passphrase
                }
            }
            else {
                sslConfig = {
                    key: fs.readFileSync(this.config.tlsKey),
                    cert: fs.readFileSync(this.config.tlsCert),
                    ca: fs.readFileSync(this.config.tlsCa),
                    requestCert: this.config.tlsRequestCert,
                    rejectUnauthorized: this.config.tlsRejectUnauthorized
                };
            }
            this.server = https.createServer(sslConfig, this.app);
        }
        else {
            this.server = http.createServer(this.app);
        }
    }


    startRecorders() : void {
        this.recorders = require(this.config.configDirectory + "/" + this.config.recorderFile);
        for(var i =0; i<this.recorders.length; i++) {
            var recorder = this.recorders[i];
            // console.dir(recorder);
            if (recorder.active==true) {
                recorder.store = this.store;
                new this.consumerClass(recorder).start();
            }
        }
    }

    onStoreConnect() : void {
        if(this.config.startRecorders && this.store) {
            console.log("Starting recorders.")
            this.startRecorders();
        }
    }

    onUpgradeRequest (request: any) {
        console.log("Upgrade Request: ");
        console.dir(request.httpRequest.headers);
        // For web clients, copy query string parameters to headers.
        for(var queryParameter in request.resourceURL.query)  {
            request.httpRequest.headers[queryParameter.toLowerCase()] = request.resourceURL.query[queryParameter];
        }

        if(this.authenticate(request)) {
            // Look for a request to re-connect a session
            let sessionId = request.httpRequest.headers['etp-sessionid'];

            if (sessionId != null) {
                let session = this.connections[sessionId];
                if (session) {
                    session.connection = request.accept(etp.WS_PROTOCOL, request.origin);
                    session.resume();
                }
                else {
                    request.reject(410);
                }
            } else {
                this.createConnection(request);
            }
        }
        else {
            request.reject(401, "Authentication failed.");
        }
    }
    
    run() {
        this.configure();

        this.createServer();

        this.server.listen(this.config.wsPort, function() {
            console.log((new Date()) + ' Server is listening on port '+ this.wsPort);
        }.bind(this.config));
        let socketServer = new ws.server({ httpServer: this.server});
        socketServer.on('request', this.onUpgradeRequest.bind(this));

    }
    
    configure() {

        var argv=optimist
            .usage("\nETP Server for Node.js")
            .default(
                {
                    exitOnClose: false,
                    wsPort: 8081,
                    simpleStreamer: false,
                    traceMessages: false,
                    traceDirectory: 'trace',
                    databaseConnectString: 'mongodb://localhost:27017/test',
                    storeModule: '../lib/providers/MongoStore',
                    providerModule: '../lib/providers/StoreProvider',
                    consumerModule: '../lib/providers/RecordingConsumer',
                    configDirectory: '../../config',
                    recorderFile: 'recorders.json',
                    authentication: 'none',
                    startRecorders: false,
                    pfxFile: '',
                    passphrase: '',
                    useTls: false,
                    tlsKey: '',
                    tlsCert: '',
                    tlsCa: '',
                    tlsRequestCert: false,
                    tlsRejectUnauthorized: false,
                    jwt: {
                        signingSecret: process.env.JWT_SIGNING_SECRET
                    }
                }
            ).argv;

        // Load the specified factory modules. The config expression must resolve to a 'new-able' object.
        if (argv.storeModule!="null") {
            this.storeClass = require(argv.storeModule);
        }
        this.providerClass =  require(argv.providerModule);
        optimist.default(this.providerClass.defaultConfig);

        this.consumerClass = require(argv.consumerModule);

        this.config = optimist.parse(process.argv);

        if (this.storeClass!=null) {
            this.store = new this.storeClass(argv);
            this.store.on("connect", this.onStoreConnect.bind(this));
        }

        if (this.config.help) {
            optimist.showHelp();
            process.exit(0);
        }

        console.log(this.config);
    }
}

export = EtpServer;