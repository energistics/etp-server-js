/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import Energistics = require("../Energistics");

/// Use the w3c-style so we can use the web-style API that ETPClient uses.
var socket = require('websocket').w3cwebsocket;

/**
 * Protocol 1 Consumer that saves it results in the store.
 */
class RecordingConsumer {
    constructor(private _config) {
        _config || (_config = {});
        _config.retryInterval || (_config.retryInterval=30000);     // The retry interval, in milliseconds if we get disconnected.
        _config.encoding || (_config.encoding="binary");            // encoding to be used for the session.
        _config.url || (_config.url="ws://localhost:8080");         // The url and port for the producer.
        _config.uri || (_config.uri="/");                           // The uri of channels to be described on connect.
        _config.channelPattern || (_config.channelPattern="^.*$");// regex specifying the channelNames we care about.

        this._etp = new Energistics.ETPClient(_config);
        this._etp.on('log', console.log);
        this._etp.on('connect', this.onSocketConnect.bind(this));
        this._etp.on('disconnect', this.onSocketDisconnect.bind(this));
        this._etp.on('close', this.onCloseSession.bind(this));
        this._etp.on('open', this.onOpenSession.bind(this));
        this._etp.registerHandler(1, new Energistics.ChannelStreamingConsumer(this._etp));
        this._etp.handlers[1].on('metadata', this.onChannelMetadata.bind(this));
        this._etp.handlers[1].on('data', this.onChannelData.bind(this));
    }

    private _etp : Energistics.ETPClient;       // Our client session
    private _connected : boolean = false;       // Means we are currently connected
    private _running : boolean = false;         // Means we want to be connected

    /// A list of local channel metadata, sparse array by channelId
    /// These ids are private because they are the Ids provided
    /// by the producer.
    _channels = [];

    _channelBuffers = [];

    /// Connect to the websocket server.
    private connect() {
        if (this._running) {
            this._etp.connect(this._config, socket);
        }
    }

    /// Handle channel data messages.
    private onChannelData(msg) {
        var message = msg.body;
        var data = message.data;
        //console.log("Received [" + message.data.length + "] points from " + this._config.url);
        // There must be at least one index supplied
        if(this._config.storeAsChannelSet) {
            this._config.store.putIndexAlignedValues(this._channels, this._config.contextUri, data);
        }
        else {
            var indexes = message.data[0].indexes;
            for (var i = 0; i < data.length; i++) {
                var channelId = data[i].channelId;
                var thisChannel = this._channels[channelId];
                if (thisChannel !== undefined) {
                    // process.stdout.write(thisChannel.channelId+" ");
                    // Give each point its own index when we present to the database.
                    if (data[i].indexes.length > 0) {
                        // This point has an index, so make it the default for future points
                        indexes = data[i].indexes;
                    }
                    else {
                        // No index, so take the current default.
                        data[i].indexes = indexes;
                    }
                    // Send it to the store.
                    var buffer = this._channelBuffers[channelId];
                    if (buffer.length == 60) {
                        this._channelBuffers[channelId] = [];
                        this._config.store.putChannelValues(thisChannel, buffer);
                    }
                    this._channelBuffers[channelId].push(data[i]);
                }
                else {
                    // note that this is likely to occur for simpleStreamers, since we will start receiving data before
                    // we have registered the channel in the database.
                    console.log("Received data point for unregistered channel: " + channelId);
                }
            }
        }
    }

    public get connectedToSimpleStreamer(){
        return (this._etp.serverProtocols[1].protocolCapabilities["simpleStreamer"]!=undefined);
    }

    /// Handle channel metadata messages.
    private onChannelMetadata(msg) {
        var message = msg.body;
        for (var i = 0; i < message.channels.length; i++) {
            var thisChannel = message.channels[i];
            this._channels[thisChannel.channelId] = thisChannel;
            this._channelBuffers[thisChannel.channelId] = [];
            if (this._config.contextUri) {
                thisChannel.channelUri = this._config.contextUri + "/" + thisChannel.channelUri;
                thisChannel.contextUri = this._config.contextUri;
            }
            this._config.store.addChannel(thisChannel, function(err, data){
                // We only need to start streaming explicitly if the producer is not a simpleStreamer
                if (! this.consumer.connectedToSimpleStreamer) {
                    if (this.thisChannel.channelName.match(this.consumer._config.channelPattern) ) {
                        var csi = new Energistics.Datatypes.ChannelData.ChannelStreamingInfo();
                        csi.channelId = this.thisChannel.channelId;
                        csi.receiveChangeNotification = true;
                        (<Energistics.ChannelStreamingConsumer>(this.consumer._etp.handlers[1])).channelStreamingStart([csi]);
                    }
                }

            }.bind({consumer: this, thisChannel: thisChannel}));

        }
    }

    private onCloseSession() {
        this._channels = [];
        this._channelBuffers = [];
    }

    private onOpenSession() {
        this._channels = [];
        this. _channelBuffers = [];
        // Only need to do a channel describe if it is not a simpleStreamer
        if (!this.connectedToSimpleStreamer) {
            (<Energistics.ChannelStreamingConsumer>(this._etp.handlers[1])).describe([this._config.uri]);
        }
    }

    onSocketConnect(socket) {
        this._connected = true;
        console.log('Socket Status: ' + socket.readyState + ' (open)');
        console.log("Requesting session with {" + this._config.url + "}");
        var thisVersion = {major: 1, minor: 0, revision: 0, patch: 0};
        this._etp.requestSession([
            { protocol: 1, protocolVersion: thisVersion, role: "producer", protocolCapabilities: {}  }
        ], "ralf-recording-client");
    }

    /// Socket was disconnected.
    onSocketDisconnect(socket) {
        this._connected = false;
    }

    // Periodically try to reach the producer if the connection goes down.
    private retry() {
        if(!this._connected && this._running) {
            console.log("Trying to connect to: " + this._config.url);
            this.connect();
        }
    }

    /// Control command to start the recorder
    start() {
        this._running = true;
        this.connect();
        // Ignore retry intervals of less than a second
        if(this._config.retryInterval > 1000) {
            setInterval(this.retry.bind(this), this._config.retryInterval);
        }
    }

    /// Control command to stop the recorder.
    stop() {
        this._running = false;
        if(this._connected) {
            this._etp.closeSession();
        }
    }

}

export = RecordingConsumer;
