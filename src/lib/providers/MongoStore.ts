/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import common = require('../common/EnergisticsCommons');
import energistics = require("../Energistics");
import etp = require("etp");
import events = require("events");
import mongo = require("mongodb");

var messages = etp.Energistics.Protocol;
var uuid = require("node-uuid");

// Some useful shorthands for types.
import DataItem = etp.Energistics.Datatypes.ChannelData.DataItem;
import {EtpUri} from "../common/EtpUri";

var WITSML_ROOT:string = "eml:///witsml1411";

function reportError(err){
    if(err) {
        console.log(err);
    }
}

function detach(source, target, name) {
    target[name]=source[name];
    source[name]=[];
}

class MyResource extends etp.Energistics.Datatypes.Object.Resource{
    constructor(public uri: string, public name: string, public resourceType = "DataObject", public channelSubscribable=false)
    {
        super();
        this.hasChildren = -1;
        this.customData = {};
    }
}

// this is a basic projection for the mongodb aggregation pipeline
// that will convert a document from any collection to an ETP Resource
// struct.
class SimpleProjection   {
    _id: number = 0;
    uid : any = {$substr: ["$_id",0,-1]};
    uri : any = { "$concat": [ WITSML_ROOT, "/obj_", "(", "$_id", ")"] };
    name: string = "$name";
    channelSubscribable: any = { "$add" : 1 };
    contentType: any = { $concat: ['application/x-witsml+xml;version=1.4.1.1;type=obj_'] };
    customData: any = {};
    resourceType: any =  { $concat: ['DataObject'] };
    hasChildren: any = { $add: [-1]};
    lastChanged: any = { time: "$commonData.dTimLastChange.date", offset: { $add: [0]} };
    constructor(objectType: string) {
        this.contentType.$concat[0] += objectType;
        this.uri.$concat[1] += objectType;
    }
}

class ChannelProjection   {
    _id: number = 0;
    uid : any = {$substr: ["$_id",0,-1]};
    uri : any = { "$concat": [ WITSML_ROOT, "/obj_channel", "(____)"] };
    name:string = "$channelName";
    channelSubscribable: any = { "$add" : 1 };
    contentType: any = { $concat: ['application/x-witsml+xml;version=2.0;type=obj_channel'] };
    customData:any = {};
    resourceType:any = {$concat: ['DataObject']};
    hasChildren:any = { $add: [0] };
    lastChanged: any = { time: "$commonData.dTimLastChange.date", offset: { $add: [0]} };

    constructor(objectType: string ) {
    }
}

function nonEmptyString(s) {
    return s && ( s.length > 0 );
}

class MongoStore extends events.EventEmitter implements common.IStore, common.IChannelStore {
    wells = null;
    wellbores = null;
    logs =  null;
    logrecords = null;
    trajectorys = null;
    trajectoryStations = null;
    channels = null;
    mudLogs = null;
    channelData = null;
    counters = null;
    objectNotification = null;
    db = null;

    /// Database connection statistics for one server session.
    private stats: any ={
        /// Number of points successfully loaded (counted after callback).
        pointsLoaded: 0,
        /// Number of points queued for loading.
        pointsQueued: 0,

        /// Number of pending batches to be resolved.
        batches: 0,

        /// Number of row-oriented inserts of log data.
        rowsLoaded: 0,

        rowsQueued: 0
    };


    constructor(config: any) {
        super();
        console.log("Connecting to database: " + config.databaseConnectString);
        mongo.MongoClient.connect(config.databaseConnectString, this.on_connect.bind(this));
    }

    //<editor-fold desc="Implement IChannelStore">

    /// Upsert a new channel into the store.
    //  Matching semantics are currently based on uri + source.
    /// MUST be called before adding or fetching datapoints. It will add additional channel metadata that mongo will use
    /// optimize storage.
    addChannel(channelInfo, callback) {
        // this.setPageSize(channelInfo);
        delete channelInfo.startIndex;
        delete channelInfo.endIndex;
        delete channelInfo._id;
        this.channels.findOneAndUpdate({channelUri: channelInfo.channelUri}, channelInfo, {returnOriginal:true}, function(err, doc){
            if (doc.value!=null){
                this.channelInfo._id = doc.value._id;
                if (this.callback) {
                    this.callback(err, doc);
                }
            }
            else {
                this.store.getNextId('channelId', function(err, doc){
                    this.channelInfo._id = doc.value.seq;
                    this.channelInfo.startIndex = 9e99;
                    this.channelInfo.endIndex = -9e99;
                    this.store.channels.insertOne(this.channelInfo, function(err, data){
                        if(this.callback)
                            this.callback(err, data.ops[0]);
                    }.bind(this));
                }.bind(this));
            }
        }.bind({store:this, channelInfo: channelInfo, callback: callback}));
    }

    /// Convert a point as stored in Mongo to ETP DataItem form.
    dbToEtp(channelInfo, point) {
        return {
            channelId: channelInfo._id,
            indexes: [ this.dbToEtpIndex(channelInfo.indexes[0], point.index) ],
            value: this.dbToEtpValue(channelInfo, point.value),
            valueAttributes: { }
        }
    }

    dbToEtpIndex(indexInfo, value) : any {
        return common.encodeIndexValue(indexInfo, value);
    }

    dbToEtpValue(channelInfo, value) : any {
        //TODO - handle more cases, proper conversion on insert.
        switch (channelInfo.dataType) {
            case "date time":
            case "DateTime":
            case "long":
                return {
                    item: {
                        'long': value
                    }
                };
            default:
                return { item: {double: value  } };
        }
    }


    getChannelRange(channelInfo: any, startIndex: number, endIndex: number, callback) {
        var startValue =  this.indexValue(startIndex);
        var endValue =  this.indexValue(endIndex);
        var fields = {};
        fields[channelInfo._id]=1;
        fields["indexValue"] = 1;
        this.channelData.find({
                $and: [
                    // For only the channel we care about
                    { contextUri: channelInfo.contextUri },
                    { indexValue: {$gte: startValue} } ,
                    { indexValue: {$lte: endValue } }
                ]
            },
             fields,
            { sort: { indexValue:1 } }
        ).toArray(function(err, result){
                var resultPoints = [];
                if(!err) {
                    for (var i = 0; i < result.length; i++) {
                        resultPoints.push(this.store.dbToEtp(this.channelInfo, {index: result[i].indexValue, value: result[i][this.channelInfo._id]}));
                    }
                }
                if(this.callback) {
                    this.callback(err, resultPoints)
                }
            }.bind({callback: callback, channelInfo: channelInfo, store: this}));
    }

    /// Describe all channels. Finds Active and Inactive, but not Closed.
    describeChannels(uris: string[], callback) {

        this.channels.find({status: {$ne: "Closed" } }).toArray(function(err, data){
            for (var i = 0; i < data.length; i++) {
                var channelMetadata = data[i];
                channelMetadata.startIndex = common.encodeIndexValue(channelMetadata.indexes[0], channelMetadata.startIndex);
                channelMetadata.endIndex = common.encodeIndexValue(channelMetadata.indexes[0], channelMetadata.endIndex );
                channelMetadata.channelId = channelMetadata._id;
                channelMetadata.scale = 5;
                if (!channelMetadata.measureClass)
                    channelMetadata.measureClass = "";
                if (!channelMetadata.source)
                    channelMetadata.source = "";
            }
            if (callback) {
                callback(err, data);
            }
        }.bind({callback: callback, store: this}));
    }


    putChannelValue(channelInfo: any, value: any, callback = null) {
        var target = this.channelData;
        if (channelInfo._id) {
            value.channelId = channelInfo._id;
            var channel_id = channelInfo._id;
            var indexValue = this.indexValue(value.indexes[0]);
            var itemName = Object.getOwnPropertyNames(value.value.item)[0];
            var pointValue = value.value.item[itemName];

            var point = {};
            point[channel_id] = pointValue;

            target.updateOne(
                { contextUri: channelInfo.contextUri, indexValue: indexValue },
                { $set: point  },
                { upsert: true },
                function(err, data){
                    if (callback) {
                        callback(err, data);
                    }
                    this.stats.pointsLoaded++;
                }.bind({callback: callback, stats:this.stats })
            );

            this.channels.update(
                {_id: channel_id },{
                    $set: { latestValue: pointValue  },
                    $min: { startIndex: indexValue },
                    $max: { endIndex: indexValue }
                }, {}, reportError
            );

            this.stats.pointsQueued++;
            this.emit('channel-point', null, value);

        }
        else {
            // console.log("Channel " + channelInfo.channelId + " has no _id");
        }
    }

    // stores a group of related channel values at a common index point. The channel metadata must be a MAP from
    // internal channel id to its metadata, NOT an array.
    putIndexAlignedValues(channelMetadata, options, points, callback=null) {
        this.stats.pointsQueued += points.length;
        this.stats.rowsQueued++;
        var doc : any = { contextUri: "" };
        var indexValue = this.indexValue(points[0].indexes[0]);
        doc["indexValue"] = indexValue;

        for (var i=0; i<points.length; i++) {
            var thisChannel = channelMetadata[points[i].channelId];
            doc.contextUri = thisChannel.contextUri;
            var itemName = Object.getOwnPropertyNames(points[i].value.item)[0];
            var pointValue = points[i].value.item[itemName];
            points[i].channelId = thisChannel._id;

            // This code puts a given point into the mongo document. We've played with using both channelId and
            // channelName as the identifier. Using channelName implies they are unique across channel sets. If you
            // change this, you need to change the logic in the reading code as well.
            doc[points[i].channelId]=pointValue;
            // doc[thisChannel.channelName]=pointValue;

            if (!options || (options.bulk!=true))
                this.channels.update(
                    {_id: thisChannel._id },{
                        $set: { latestValue: pointValue  },
                        $min: { startIndex: indexValue },
                        $max: { endIndex: indexValue }
                    }, {}, reportError
                );

        }
        this.emit('channel-points', null, points);
        this.channelData.updateOne({ indexValue: indexValue, contextUri: doc.contextUri }, doc, {upsert: true}, function(err, result){
            this.stats.pointsLoaded += points.length;
            this.stats.rowsLoaded++;
            if(this.callback) {
                this.callback(err, result);
            }
        }.bind({callback: callback, stats: this.stats, points: points}));
        //console.dir(doc);
    }

    putIndexAlignedValuesBatch(channelMetadata, options, rows, callback=null) {
        if(rows.length>0){
            var batch = this.channelData.initializeUnorderedBulkOp();
            for (var rowNumber=0; rowNumber<rows.length; rowNumber++){
                var points = rows[rowNumber];
                var doc : any = { contextUri: "" };
                var indexValue = this.indexValue(points[0].indexes[0]);
                doc["indexValue"] = indexValue;
                this.stats.pointsQueued += points.length;
                this.stats.rowsQueued++;
                doc.pointCount = points.length;
                for (var i=0; i<points.length; i++) {
                    var thisChannel = channelMetadata[points[i].channelId];
                    doc.contextUri = thisChannel.contextUri;
                    var itemName = Object.getOwnPropertyNames(points[i].value.item)[0];
                    var pointValue = points[i].value.item[itemName];
                    points[i].channelId = thisChannel._id;

                    // This code puts a given point into the mongo document. We've played with using both channelId and
                    // channelName as the identifier. Using channelName implies they are unique across channel sets. If you
                    // change this, you need to change the logic in the reading code as well.
                    doc[points[i].channelId]=pointValue;
                    // doc[thisChannel.channelName]=pointValue;
                }
                batch.insert(doc);
                this.stats.pointsLoaded += points.length;
                this.stats.rowsLoaded++;
            }

            batch.execute({}, function(err, result){
                if(this.callback) {
                    callback(err, result);
                }
            }.bind({callback: callback, stats: this.stats, points: points}));
        }
        else {
            console.log("No rows in dataset.");
            if(callback) {
                callback(null, null);
            }
        }
    }

    putChannelValues(channelInfo, points, callback=null)
    {
        var did_one = false;
        var pointCount = points.length;
        if(channelInfo._id) {
            this.emit('channel-points', null, points);
            var batch = this.channelData.initializeUnorderedBulkOp();
            var channel_id = channelInfo._id;
            this.stats.pointsQueued += pointCount;
            this.stats.batches++;
            var buckets = this.bucketPoints(channelInfo, points);

            for(var bucket in buckets) {
                var page = Number(bucket) - 0;
                points = buckets[bucket];
                did_one = true;
                batch.find({ uidChannel: channel_id, page: page }).upsert().updateOne(
                    {
                        $set: {
                            uidChannel: channel_id,
                            page: page
                        },
                        $push: {values: { $each: points } }
                    }
                );
            }
        }

        // batch execution fails if there isnt at least one command.
        if (did_one) {
            batch.execute( function(result){
                //console.dir(result);
                this.stats.pointsLoaded += this.pointCount;
                this.stats.batches--;
                if(callback){
                    callback(null, result);
                }
            }.bind({callback: callback, stats: this.stats, pointCount: pointCount}));
        }
        else {
            if(callback){
                console.log("No buckets for " + points.length + " points!");
                callback(null, null);
            }

        }

    }

    /// Group an array of points into buckets by their page number in the channelData
    private bucketPoints(channelInfo, points) {
        var buckets={};
        for(var i=0; i<points.length; i++) {
            var indexValue = this.indexValue(points[i].indexes[0]);
            var page = this.calcPage(channelInfo, indexValue);
            if (!buckets[page])
                buckets[page]=[];
            var itemName = Object.getOwnPropertyNames(points[i].value.item)[0];
            var pointValue = points[i].value.item[itemName];

            var point = {
                index: this.indexValue(points[i].indexes[0]),
                value: pointValue
            };
            buckets[page].push(point);
        }
        return buckets;
    }

    /// Get the primary index value from a data item as a simple number.
    private indexValue(index: number): number {
        return index;
    }

    /// Calculates the page# for a given data point, based on its value and pageSize.
    private calcPage(channelInfo: any, value: number): number {
        return Math.floor(value / channelInfo.pageSize);
    }

    /// Calculates the dataPoint page size for a given channel and adds it to the channelMetadata.
    private setPageSize(channelInfo: any) {
        var indexType = channelInfo.indexes[0].indexType;
        switch (indexType) {
            case "Time":
            case "ElapsedTime":
                channelInfo.pageSize = (1000000 * 60); // One minutes's worth of data in microseconds
                break;
            default:
                channelInfo.pageSize = 100000;
                break;
        }
    }

    private pageSpan(channelInfo: any, minIndex, maxIndex) {
        return { start: this.calcPage(channelInfo, minIndex), end: this.calcPage(channelInfo, maxIndex) }
    }

    //</editor-fold>

    connect(connectString: string) {
    }

    /// Service the delete object message from the store protocol.
    delete(uriString:string)
    {
        var uri = new EtpUri(uriString);
        if(uri.identifier && uri.collection) {
            var mongoId = uri.identifier;
            this[uri.collection+"s"].findOneAndDelete({"_id": mongoId}, {}, function(err, result) {
                if (err) {
                    console.log(err);
                }
                else {
                    // console.log(result);
                    if (result && result.value){
                        var resource = energistics.DataObjectFactory.resourceFromNative(result.value);
                        this.notify("Delete", resource, new Date());

                    }
                }
            }.bind(this));
        }
    }

    /// Service the GetChildren message from the discovery protocol
    enum(uriString: string, callback:(err, data) => void) {

        var uri = new EtpUri(uriString);

        if(uri.isRoot) {
            callback(null, [
                new MyResource(WITSML_ROOT,  'WITSML Store', "UriProtocol")
                /*, new MyResource( 'eml://resqml20',  'RESQML Earth Model', "UriProtocol")*/
            ]);
        }
        else if(uri.isWitsml) {
            if (uriString == WITSML_ROOT) {
                //var wuri = new EtpUri(uri);
                callback(null, [new MyResource(WITSML_ROOT + '/obj_well', 'Wells', "Folder")]);
                callback(null, [new MyResource(WITSML_ROOT + '/obj_channel', 'Channels', "Folder")])
            }
            else if (uri.collection=="channel") {
                if(uri.identifier) {
                }
                else {
                    this.channels.aggregate([
                        {$match: {status: { $ne : "Closed"}}},
                        {$project: new ChannelProjection("channel")}
                    ]).toArray(callback);
                }
            }
            else if (uri.collection=="well") {
                if(uri.identifier) {
                    console.log("Well:" + uri.identifier);
                    this.wellbores.aggregate([
                        { $match: { uidWell:  uri.identifier } },
                        { $project: new SimpleProjection("wellbore")}
                    ]).toArray(callback);
                }
                else {
                    this.wells.aggregate([
                        {$project: new SimpleProjection("well")}
                    ]).toArray(callback);
                }
            }
            else if (uri.collection=="wellbore") {
                var wellboreId =  uri.identifier;
                console.log("Wellbore:" + wellboreId);
                this.logs.aggregate([
                    { $match: { uidWellbore: wellboreId } },
                    { $project: new SimpleProjection("log")}
                ]).toArray(callback);
                this.trajectorys.aggregate([
                    { $match: { uidWellbore: wellboreId } },
                    { $project: new SimpleProjection("trajectory")}
                ]).toArray(callback);
                this.mudLogs.aggregate([
                    { $match: { uidWellbore: wellboreId } },
                    { $project: new SimpleProjection("mudLog")}
                ]).toArray(callback);
            }
            else if (uri.collection=="log") {
                var logId =  uri.identifier ;
                console.log("Log:" + logId);
                this.channels.aggregate([
                    { $match: { contextUri: uriString } },
                    { $project: new ChannelProjection('channel')}
                ]).toArray(callback);
            }
        }
    }

    /// Service the GetObject message from the Store protocol.
    get(uriString: string, callback:(err, data) => void) {
        var uri = new EtpUri(uriString);
        if (uri.isWitsml && uri.identifier) {
            this[uri.collection+"s"].aggregate([
                { $match: { _id: uri.identifier  } },
            ]).toArray(function(err, data){
                if(callback) {
                    if (err) {
                        callback(err, null);
                    }
                    else if (data.length > 0) {
                        callback(null, {
                            dataObject: energistics.DataObjectFactory.dataObjectFromNative(data[0]),
                            native: data[0]
                        })
                    }
                    else {
                        callback(null, null);
                    }
                }
            });
        }
    }

    getHistory(uri: string, callback:(err, data) => void) {
        return this.logrecords.find({ParentId: uri}).sort({_id: 1});
    }

    /// generate a sequence.
    getNextId(name, callback, count) {
        count || (count=1);
        this.counters.findAndModify( { _id: name }, null, { $inc: { seq: count } }, { new: true, upsert: true } , callback );
    }

    /// Services the PutObject message from the Store protocol.
    put(messageData: any, callback)
    {
        var dataObject = messageData.nativeObject;
        var resource = messageData.body.data.resource;
        var ct = new energistics.EtpContentType(resource.contentType);
        if (ct.valid) {
            var collection = ct.dataType.substr(4) + "s";
            if(dataObject==null) {
                var xml = new energistics.DataObjectFactory(resource.contentType);
                var dataObject = xml.deserializeString(messageData.body.data.data);
                if (ct.hasPlural) {
                    // This will be the plural name, same as the collection name in mongo.
                    var collectionName = dataObject.name.localPart;

                    // Wack off the 's'
                    var singularName = collectionName.substr(0, collectionName.length - 1);

                    // This will be a collection, but will normally have a length of 1
                    var singularCollection = dataObject.value[singularName];
                    dataObject = singularCollection[0];
                }
            }
            switch (ct.dataType) {
                case "obj_log":
                    detach(dataObject, messageData, "logCurveInfo");
                    detach(dataObject, messageData, "logData");
                    break;
                case "obj_trajectory":
                    detach(dataObject, messageData, "trajectoryStation");
                    break;
            }
            dataObject._id = dataObject.uid;
            this.db.collection(collection).updateOne({_id: dataObject._id }, dataObject, {upsert: true}, function(err, data){
                if(callback) {
                    callback(err, data);
                }
                this.notify("Upsert", resource, new Date());
            }.bind(this));
        }
    }

    /// Store an object already parsed from XML.
    putNative(collection, object, callback) {
        object._id = object.uid;
        this[collection].insert(object, callback);
    }

    notify(changeType, resource, changeTime) {
        var dataObject = {resource: resource, contentType: "", contentEncoding: ""};
        this.objectNotification.insertOne({time: changeTime, changeType: changeType, dataObject: dataObject});
    }

    notificationRequest(messageData, callback) {
        var coll = this.objectNotification;
        var latestCursor = coll.find({}).sort({$natural: -1}).limit(1);
        latestCursor.nextObject(function (err, latest) {
            var conditions:any = {};
            if (latest) {
                conditions._id = {$gt: latest._id};
            }
            var options = {
                tailable: true,
                sort: {$natural: 1},
                awaitdata: true,
                numberOfRetries: -1
            };
            var cursor = coll.find(conditions, options);
            var next = function (err, doc) {
                if (err) {
                }
                else if (doc) {
                    latest = doc;
                    // Convert the date time to etp format.
                    doc.changeTime = common.etpDate(doc.changeTime);
                    callback(doc);
                }
                setImmediate(more);
            }.bind(this);

            var more = function () {
                cursor.nextObject(next);
            };

            more();

        }.bind(this));
    }

    on_connect(err, db) {
        if(err) { console.dir(err); }
        this.db=db;
        db.createCollection("objectNotification", {capped:true, size:10000000, max:10000, w:1},function(err, data){
            if(err){
                console.dir(err)
            }

            db.collection("objectNotification").insertOne({_id: "dummy"});
        });
        this.objectNotification = db.collection("objectNotification");
        this.wells = db.collection('wells');
        this.wellbores = db.collection('wellbores');
        this.logs = db.collection('logs');
        this.trajectorys = db.collection('trajectorys');
        this.trajectoryStations = db.collection('trajectoryStations');
        this.logrecords = db.collection('logrecords');
        this.channels = db.collection('channels');
        this.channels.ensureIndex({contextUri: 1},{unique: false});
        this.channelData = db.collection('channelData');
        //this.channelData.ensureIndex({uidChannel: 1, page: 1},{unique: true});
        this.channelData.ensureIndex({indexValue: 1},{unique: false});
        this.channelData.ensureIndex({contextUri: 1},{unique: false});
        this.mudLogs = db.collection('mudLogs');
        this.counters = db.collection('counters');

        this.emit('connect');

        setInterval(this.reportStats.bind(this), 10000);
    }

    reportStats() {
        // console.dir(this.stats);
    }
}

export = MongoStore;