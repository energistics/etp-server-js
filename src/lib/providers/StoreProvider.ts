/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */


import events = require("events");
import Energistics = require("../common/EnergisticsCommons");

// Protocol 0 producer based on a peristent channel store. Should work with any class
// that implements the IChannelStore interface.
class StoreProvider extends events.EventEmitter {

    constructor(private config:any, private store: Energistics.IChannelStore) {
        super();

        // subscribe to events on the database, forward to consumers
        store.on("channel-point", this.onChannelData.bind(this));
        store.on("channel-points", this.onChannelDataBlock.bind(this))
    }

    // Sparse map of channel Ids to metadata for channels we have described.
    // Assume that channelId is provided by the store, but we have a private copy
    // of the ChannelMetadata record.
    private channelMetadata = {};

    /// Sparse map of channelIds to ChannelRangeInfo records that are sent to start streaming.
    private activeChannels: any={};

    /// This realtime provider is active. Once this goes to false, the class cannot be reused.
    private running: boolean = true;


    /// Service the ChannelRangeRequest message.
    rangeRequest(msg: any) {
        var message = msg.body;
        for(var i=0; i<message.channelRanges.length; i++) {
            var channelRange = message.channelRanges[i];
            for(var j=0; j<channelRange.channelId.length; j++) {
                var channelId = channelRange.channelId[j];
                var channelInfo = this.channelMetadata[channelId];
                if (channelInfo) {
                    this.store.getChannelRange(channelInfo, channelRange.startIndex, channelRange.endIndex, function(err, data){
                        if(!err) {
                            this.provider.emit('data', { data: data }, this.message.header.messageId);
                        }
                    }.bind({provider: this, message: msg}));
                }
            }
        }
    }

    /// Service the ChannelStreamingStart message
    startStreaming(channels: any[]) {
        for(var i=0; i<channels.length; i++) {
            var csi = channels[i];
            this.activeChannels[csi.channelId]=csi;
        }
    }

    /// Stop the provider completely
    stop() {
        if (this.running) {
            this.running = false;
            this.activeChannels = [];
            this.store.removeListener("channel-point", this.onChannelData.bind(this));
        }
    }

    /// Service the ChannelStreamingStop message
    stopStreaming(channels) {
        for(var i=0; i<channels.length; i++) {
            delete this.activeChannels[channels[i]];
        }
    }

    handleObject(err: any, data: any) {
        // Todo
    }

    /// Channel describe
    describe(msg) {
        this.store.describeChannels(msg.body.uris, function (err, data) {
            for (var i = 0; i < data.length; i++) {
                var channelMetadata = data[i];
                this.provider.channelMetadata[channelMetadata.channelId]=channelMetadata;
                this.provider.emit('metadata', {channels: [channelMetadata] }, this.request.header.messageId);
            }
        }.bind({provider: this, request: msg}));
    }

    /// Notification of a single new data point in store
    onChannelData(err, point) {
        if (this.activeChannels[point.channelId]) {
            this.emit('data', { data: [point] });
        }
    }

    /// Notification of multiple data points from store
    onChannelDataBlock(err, data) {
        if(err) {
            //TODO
        }
        else if ((data!=null) && (data.length>0) && (this.running==true)) {
            //console.log(data.map(function(p){ return p.channelId }));
            var myData = data.filter(function(point){
                return this.activeChannels[point.channelId]!=null;
            }.bind(this));
            if(myData.length>0) {
                myData[0].indexes[0] = data[0].indexes[0];
                this.emit("data", {data: myData});
            }
        }
    }
}

export = StoreProvider;