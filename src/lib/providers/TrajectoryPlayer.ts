/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import events = require("events");
import Energistics = require("../Energistics");
import common = require("../common/EnergisticsCommons");
import path = require("path");
import fs = require("fs");

class TrajectoryPlayer extends events.EventEmitter {
    trajectory: any;
    stations: any;
    doc: any;
    channelId: number = 1;              // As a stand-alone server, we can always use channel id 1
    running: boolean = false;           // Says we are actively pushing points
    currentRow: number = 0;             // The index of the row about to be pushed.
    totalRows: number = 0;              // Total number of trajectory stations in the trajectory

    // The one and only index metadata record. Always sends time in microseconds.
    indexMetadata: any = {indexType: "Time", mnemonic:"Time", description: "Time", uri: "Time", direction: "Increasing", uom: "ms"}; //TODO - microsec

    constructor(private _config: any) {
        super();
        _config.fileName || (_config.fileName = "");
        let xmlString = fs.readFileSync(_config.fileName, "utf-8");

        // Xml helper class for the JSonix serializers
        var xml : Energistics.DataObjectFactory =new Energistics.DataObjectFactory("application/x-witsml+xml;version=1.4.1.1;type=obj_trajectory");

        // Turn the XML file into a js object.
        this.doc=xml.deserializeString(xmlString);

        // The one and only trajectory (doc itself is a plural object)
        this.trajectory = this.doc.value.trajectory[0];

        // Separate the main trajectory object from the stations, so that we can use it as a wrapper for one station at a time.
        this.stations = this.trajectory.trajectoryStation;
        this.trajectory.trajectoryStation=[];
        this.totalRows = this.stations.length;
    }

    /// Sends the next available station.
    send() {
        if (this.running) {
            // Get the next trajectory station in array order.
            var row = this.stations[this.currentRow];
            if(row) {
                // Serialize it to an Xml string fragment.
                this.doc.value.trajectory[0].trajectoryStation[0]=row;
                var xml = new Energistics.DataObjectFactory("application/x-witsml+xml;version=1.4.1.1;type=obj_trajectory");
                var data = xml.serializeString(this.doc.value.trajectory[0]);
                // Note t0 and t1 will be in ms, irrespective of etp date format.
                var t0 = row.dTimStn.date.valueOf();
                var index = common.etpDate();
                // emit a single data point, on channel 1, which is the trajectory station.
                this.emit("data", { data: [ {
                    channelId: 1,
                    indexes:[index],
                    value: { item: {string: data } },
                    valueAttributes: null }]
                });
                if (++this.currentRow < this.totalRows) {
                    var nextRow = this.stations[this.currentRow];
                    var t1 = nextRow.dTimStn.date.valueOf();
                    if(this._config.speed <= 0) {
                        this.send();
                    }
                    else {
                        setTimeout(this.send.bind(this), (t1-t0) / this._config.speed);
                        console.log("Delaying: " + (t1-t0) / this._config.speed / 1000.0 + "sec.");
                    }
                }
                else {
                    if(this._config.continuous) {
                        this.currentRow = 0;
                        this.send();
                    }
                    else {
                        this.stop();
                    }
                }
            }
        }
    }

    channelStreamingStart() {
        this.running = true;
        this.send();
    }

    stop() {
        this.running = false;
    }

    describe(msg) {
        if(msg.body.uris[0]=="/") {
            if (this.trajectory && (this.totalRows>0)) {
                this.emit('metadata', {  channels: [{
                    channelId: this.channelId,
                    channelUri: "eml:///witsml1411/obj_trajectory(" + this.trajectory.uid + ")",
                    indexes: [this.indexMetadata],
                    dataType: "byte[]",
                    uom: "",
                    startIndex: null,
                    endIndex: null,
                    channelName: "Trajectory",
                    description: this.trajectory.name,
                    status: "Active",
                    source: "",
                    measureClass: "",
                    contentType: "application/x-witsml+xml;version=1.4.1.1;type=obj_trajectory",
                    uuid: ""
                }] }, msg.header.messageId);
            }
        }
        else {

        }
    }
}

export = TrajectoryPlayer;