/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

/// Generate points from windows perf counters. Only works on windows, duh.

var perfmon = require("perfmon");
var mkdirp = require("mkdirp");
var uuid = require("node-uuid");

import path = require("path");
import fs = require("fs");
import events = require("events");
import { etpDate, IStore} from "../common/EnergisticsCommons";
import { Energistics } from "etp";
import { EtpUri } from "../common/EtpUri";

// Use the root of our install to create configuration.
var appRoot = path.dirname(path.dirname(module.filename));

var WITSML_ROOT="eml:///witsml20";

/// Array of active sessions. Contains PerfMonSimulator objects.
var m_sessions=[];

/// Array of Channel Names
var m_channels=[];

/// Hash of channels by name, with state information.
var m_reverse_index={};

var m_host;

/// Base call back for the main perfmon server
function newData(err, data:any) {
    // console.log(data.time);
    // console.log("Sending to " + m_sessions.length + " sessions.");
    // pass on to all active sessions.
    for (var i=0 ; i < m_sessions.length; i++) {
        m_sessions[i].nextRow(err, data);
    }
}

function configure() {
    var configDir = appRoot+"/config/"+m_host;
    var configFile = configDir + "/config.json";
    mkdirp.sync(configDir);

    var config={};
    if(fs.existsSync(configFile)) {
        console.log("Re-using channel configuration file: " + configFile);
        config = require(configFile);
    }
    else {
        console.log("Creating new channel configuration file: " + configFile);
    }

    for (var channel in m_reverse_index) {
        if (config.hasOwnProperty(channel) && config[channel].hasOwnProperty("uuid")) {
            m_reverse_index[channel].uuid = config[channel]["uuid"];
        }
        else {
            config[channel] = {uuid: uuid.v4()};
            m_reverse_index[channel].uuid = config[channel].uuid;
        }
    }

    fs.writeFileSync(configFile, JSON.stringify(config));
}

function startRecording() {
    configure();
    console.log("Starting recording of " + m_channels.length + " channels.");
    perfmon(m_channels, newData);
}

/// Callback for perfmon.list. Create a channel locally, assigning a channelId
function registerChannel(err, data) {
    for(var i=0; i<data.counters.length; i++) {
        var id = m_channels.length;
        m_host = data.host;
        m_channels[id]=data.counters[i];
        m_reverse_index[data.counters[i]]={id: id, streaming: false, lastValue: null};
    }
    // Start perfmon running, whether we are listening or not.
    if (this.lastChannel) {
        startRecording();
    }
}

/// Filter for channel describe. Return true if a channel matches any of the requested Uris
function validForUri(val, index) {
    var uris = this;
    for(var i=0; i<uris.length; i++) {
        if(uris[i]=="/")
            return true;
        else {
            return (val.channelUri.indexOf(uris[i]) == 0);
        }
    }
    return true;
}

/// Select a couple of perf counter groups at random to register. Each group is 'Log'  in our server.
/// TODO - configurable.
perfmon.list('memory', registerChannel);
perfmon.list('processor', registerChannel.bind({lastChannel: true}));

/// Convenience Resouce class for the store functions.
class MyResource extends Energistics.Datatypes.Object.Resource{
    constructor(public uri: string, public name: string, public resourceType = "DataObject")
    {
        super();
        this.channelSubscribable = true;
        this.hasChildren = -1;
        this.customData = {};
    }
}

/// Implements the realtime streaming provider required by the Energistics library
export class PerfMonProvider extends events.EventEmitter {

    constructor(private sessionId: string, configuration) {
        super();
        this.config = configuration;
        m_sessions.push(this);
    }

    newChannelId = null;
    running: boolean = true;
    server: string="";
    activeChannels: any[]=[];
    lastValue: any[]=[];
    config: any = {};

    nextRow(err, data: any) {
        if(!this.running)
            return;

        var channelData: any = { data:[] };
        var time: any = new Date(data.time);
        var indexValue = etpDate(time);
        for (var counter in data.counters) {
            var channel = m_reverse_index[counter];
            var channelId = channel.id;
            var dataValue = data.counters[counter];
            if (channel && this.activeChannels[ channelId ]) {
                // Only send when value is changed.
                // console.log(channelId)
                if( (this.lastValue[channelId] != dataValue) || (!this.config.skipDuplicates)) {
                    // Save as last value.
                    this.lastValue[ channelId ]=dataValue;
                    var dataItem: any = {
                        indexes: [],
                        channelId: channel.id,
                        value: {item: {double: dataValue } },
                        valueAttributes: []
                    };
                    channelData.data.push(dataItem);
                }
            }
        }

        // If anything that we have turned on has changed since the last data point.
        if(channelData.data.length > 0) {
            // Push the common index value onto the first data point
            channelData.data[0].indexes.push(indexValue);
            // Notify listeners
            this.emit("data", channelData)
        }

    }

    startAllChannels() {
        for(var i=0; i<m_channels.length; i++) {
            var csi = new Energistics.Datatypes.ChannelData.ChannelStreamingInfo();
            csi.channelId = i;
            this.activeChannels[i]=csi;
        }
    }


    /// Start streaming for an array of channel IDs
    startStreaming(channels: Energistics.Datatypes.ChannelData.ChannelStreamingInfo[]) {
        for(var i=0; i<channels.length; i++) {
            var csi = channels[i];
            this.activeChannels[csi.channelId]=csi;
        }
    }

    /// Stop streaming for an array of channel IDs
    stopStreaming(channels) {
        for(var i=0; i<channels.length; i++) {
            delete this.activeChannels[channels[i]];
        }
    }

    stop() {
        if (this.running) {
            var index = m_sessions.indexOf(this);
            if(index>=0) {
                console.log("Removing session {" + this.sessionId + "} from sender list.");
                m_sessions.splice(index, 1);
            }
        }
        this.running = false;
    }


    /// Describe a list of URIs. Normally, send a single URI of "/" to get them all.
    describe(msg) {
        var uris = msg.body.uris;
        var channels  = m_channels.filter(validForUri, uris);
        var metadataRecords = channels.map(this.toChannelMetadata, this);
        //console.dir(metadataRecords);
        this.emit('metadata', {  channels: metadataRecords }, msg.header.messageId );
    }

    /// Transform a single metric to a metadata record.
    toChannelMetadata(uri) {

        if (m_channels.indexOf(uri) < 0)
            throw new Error("URI Does not exist");

        return {
            channelUri: uri,
            channelId: m_reverse_index[uri].id,
            // All perfmon data is time indexed.
            indexes: [
                { indexType: "Time", description: "Time", uri: {string: "Time"}, direction: "Increasing", mnemonic:  "Time", indexID: 0, uom: "ms", datum: null, customData: { } }
            ],
            dataType: "double",
            uom: "euc",
            startIndex: null,
            endIndex: null,
            channelName: uri,
            description: uri,
            status: "Active",
            contentType: null,
            source: m_host,
            measureClass: "time",
            uuid: m_reverse_index[uri].uuid
        };
    }
}

/// Simple store implementation. No CURD. Just enough to support discovery.
export class PerfStore extends events.EventEmitter implements IStore {

    constructor(connectString: string) {
        super();
    }

    connect(){ }

    /// implements GetResources
    enum(uriString: string, callback:(err, data) => void) {


        var uri = new EtpUri(uriString);
        if(uri.isRoot) {
            callback(null, [new MyResource(WITSML_ROOT,  'WITSML Store', "UriProtocol") /*, new MyResource( 'eml://resqml20',  'RESQML Earth Model', "UriProtocol")*/]);
        }
        else if(uri.isWitsml) {
            if (uriString == WITSML_ROOT) {
                //var wuri = new EtpUri(uri);
                callback(null, [new MyResource(WITSML_ROOT + '/obj_well', 'Wells', "Folder")])
            }
            else if (uri.collection=="well") {
                if(uri.identifier){
                    var retval = new MyResource(WITSML_ROOT + '/obj_wellbore('+m_host+'-1)', 'WB 1', 'DataObject');
                    retval.contentType = "application/x-witsml+xml;version=2.0;type=obj_wellbore";
                    callback(null, [retval]);
                }
                else {
                    var retval = new MyResource(WITSML_ROOT + '/obj_well(' + m_host + ')', 'Computer - '+m_host, 'DataObject');
                    retval.contentType = "application/x-witsml+xml;version=2.0;type=obj_well";
                    callback(null, [retval]);
                }
            }
            else if (uri.collection=="wellbore") {
                var retval1 = new MyResource(WITSML_ROOT + '/obj_log(memory)', 'Memory', 'DataObject');
                var retval2 = new MyResource(WITSML_ROOT + '/obj_log(processor)', 'Processor', 'DataObject');
                retval1.contentType = retval2.contentType = "application/x-witsml+xml;version=2.0;type=obj_log";
                callback(null, [retval1, retval2]);
            }
            else if (uri.collection=="log") {
                var results: MyResource[]=[];
                for (var metric in m_reverse_index) {
                    if(metric.indexOf(uri.identifier)==0) {
                        var channelId = m_reverse_index[metric].id;
                        var channel = new MyResource(WITSML_ROOT + '/obj_channel('+channelId+')', metric, 'DataObject');
                        channel.hasChildren = 0;
                        channel.contentType = "application/x-witsml+xml;version=2.0;type=obj_channel";
                        results.push(channel);
                    }
                }
                callback(null, results);
            }
        }
    }

    /// GetObject. Does nothing.
    get(uriString: string, callback:(err, data) => void) { }

    /// getHistory. Does nothing.
    getHistory(uri: string, callback:(err, data) => void) { }

    /// PutObject is not supported.
    put(obj: Energistics.Datatypes.Object.DataObject) { }

    /// Channel editing is not support.
    putChannelValue(uriChannel, index, value) {  }

    /// Delete is not supported
    delete(uriString:string) { }

    on_connect(err, db) {
        this.emit('connect');
    }

    notificationRequest(messageData, callback) {

    }
}
