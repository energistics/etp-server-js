/*
 *License notice
 *  
 * Energistics copyright 2015-
 * Energistics Transfer Protocol
 *
 * All rights of any portion thereof, shall remain with Energistics or its suppliers 
 * and shall remain subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

import events = require("events");
import * as Energistics from "../Energistics";
import witsml = require("../common/WitsmlHelper");
import fs = require("fs");

class LogPlayer extends events.EventEmitter {

    log: any=null;
    channelCount: number = 0;
    channelMetadata: any[]=[];
    running: boolean = false;
    currentRow: number = 0;
    totalRows: number = 0;
    
    static defaultConfig : any =  {
        fileName: './test/data/log1411.xml',
        speed: 1,
        defaultUri: '/'
    };


    constructor(private _config: any) {
        super();
        _config.fileName || (_config.fileName = "");
        var xml = new Energistics.DataObjectFactory("application/x-witsml+xml;version=1.4.1.1;type=obj_log");
        var str = fs.readFileSync(_config.fileName, "utf-8");
        var doc=xml.deserializeString(str);
        this.log = new witsml.LogLoader(null, doc.value.log[0], this);

        this.log.pivot = false;             // load the log data as a set of frames, not channels
        this.log.loadLog();

        this.totalRows = this.log.frameData.length;

        // the log loader makes channels 'Closed' by default.
        for (var i=0; i<this.log.channelMetadata.length; i++) {
            this.log.channelMetadata[i].status = "Active";
        }

    }

    send() {
        if (this.running && (!this.log.pivot)) {
            var row = this.log.frameData[this.currentRow];
            if(row) {
                var indexMetadata = this.log.channelMetadata[0].indexes[0];
                var t0 = Energistics.decodeIndexValue(indexMetadata, row[0].indexes[0]);
                row[0].indexes[0] = Energistics.encodeIndexValue(indexMetadata, new Date().valueOf());
                this.emit("data", { data: row });
                if (++this.currentRow < this.totalRows) {
                    var nextRow = this.log.frameData[this.currentRow];
                    var t1 = Energistics.decodeIndexValue(indexMetadata, nextRow[0].indexes[0]);
                    if(this._config.speed <= 0) {
                        this.send();
                    }
                    else {
                        var delay = this._config.speed;
                        setTimeout(this.send.bind(this), (t1 - t0) / this._config.speed);
                    }
                }
                else {
                    this.stop();
                }
            }
        }
    }

    channelStreamingStart() {
        this.running = true;
        this.send();
    }

    stop() {
        this.running = false;
    }

    describe(msg) {
        if(msg.body.uris[0]=="/") {
            if (this.log && this.log.logCurveInfo) {
                this.emit('metadata', {  channels: this.log.channelMetadata }, msg.header.messageId);
            }
        }
        else {

        }
    }
}

export = LogPlayer;